<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Respondent extends Model
{
    use HasFactory;
    protected $table = 'tblRespondent';

    protected $primaryKey = 'resp_id';

    public function answer()
    {
        return $this->hasMany(Answer::class, 'resp_id');
    }
}
