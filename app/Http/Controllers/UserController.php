<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\FacadesAuth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\CommonController;
use App\Models\Account;
use App\Models\AllowedLocation;
use App\Models\Department;
use App\Models\Permission;
use App\Models\SupportLocation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{

    public function __construct()
    {
    }
    /**
     * 
     * Survey users index
     * 
     * @param int $survey_id    survey id
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function survey_users($survey_id = 58)
    {
        $data = [];
        $survey_data = Survey::where('survey_id', $survey_id)->get()->toArray();
        $survey = (object)[
            'survey_id' => $survey_id,
            'survey_name' => $survey_data[0]['survey_name']
        ];

        $this->validateUser($survey);

        $data['survey'] = $survey;
        // $data['users'] = User::where('survey_id', $survey->survey_id)->get();
        $data['users'] = $this->getUsers($survey);
        $data['allUsers'] = User::where('survey_id', '!=', $survey->survey_id)->orWhereNull('survey_id')->get();

        $departments = Department::when(!\Auth::user()->isAdmin(), function ($q) use ($survey) { // when user is not admin, filter by department
            $q->where('survey_id', $survey->survey_id);
        })->get()->unique('name');

        $permissions = Permission::all()->unique('name');

        $locations = collect(); //?

        return view('users.survey_users', ['data' => $data, 'survey' => $survey, 'departments' => $departments, 'permissions' => $permissions, 'locations' => $locations]);
    }
    /**
     * 
     * Export users csv
     * 
     * @param \App\Models\Survey
     * @return \Illuminate\Support\Facades\Response
     */
    public function exportUsers(Survey $survey)
    {
        $users = $this->getUsers($survey);

        $fname = 'survey-' . $survey->survey_id . '-users.csv';

        $f = fopen($fname, 'w');

        // header row
        $row = [
            'First Name',
            'Last Name',
            'Username',
            'Email Address',
            'Last Login'
        ];

        fputcsv($f, $row);

        foreach ($users as $user) {
            $row = [
                $user->first_name,
                $user->last_name,
                $user->username,
                $user->email,
                $user->last_login,
            ];
            fputcsv($f, $row);
        };



        fclose($f);

        $headers = array(
            'Content-Type' => 'text/csv',
            'Cache-Control' => 'no-cache, must-revalidate'
        );

        return Response::download($fname, $fname, $headers);
    }
    /**
     * 
     * Get users
     * 
     * @param \App\Models\Survey
     * @return \App\Models\User
     */
    private function getUsers($survey)
    {
        $users = [];

        $user = \Auth::user();
        $tmpUsers = User::where('survey_id', $survey->survey_id)
            ->when(!$user->isAdmin(), function ($q) use ($user) {
                $allowed_departments = Department::where('user_id', $user->id)->get(); // get the apartments assigned to this user
                $allowed_departments = Department::where('survey_id', $user->survey_id)->whereIn('name', $allowed_departments->pluck('name'))->get(); // now get all the departments for this survey that are in the user's allowed list
                $q->whereIn('id', $allowed_departments->pluck('user_id')); // find all the users with ids in the allowed departments list
            })
            ->get();

        // $users = [];
        // foreach ($tmpUsers as $tmpUser) {
        //     if(!array_key_exists($tmpUser->user_id, $users))
        //         $users[$tmpUser->user_id]= $tmpUser;
        // }
        return $tmpUsers;
    }
    /**
     * 
     * Validate the user
     * 
     * @param \App\Models\Survey
     * @return void
     */
    protected function validateUser($survey)
    {
        CommonController::validateUser($survey->survey_id, 'surveyUsers');
    }
    /**
     * 
     * Get the user data
     * 
     * @param \App\Models\User
     * @return \App\Models\User
     */
    public function getUser(User $user)
    {
        $all_departments = Department::all()->unique('name');
        $user_departments = Department::where('user_id', $user->id)->get();

        $departments = [];
        foreach ($all_departments as $index => $department) {
            $departments[] = [
                'id' => $index,
                'text' => $department->name,
                'selected' => $user_departments->where('name', $department->name)->count() > 0 ? true : false
            ];
        };
        $keys = array_column($departments, 'text');
        array_multisort($keys, SORT_ASC, $departments);
        $user->departments = $departments;

        $all_permissions = Permission::all()->unique('name');
        $user_permissions = Permission::where('user_id', $user->id)->get();

        $permissions = [];
        foreach ($all_permissions as $index => $permission) {
            $permissions[] = [
                'id' => $index,
                'text' => $permission->name,
                'selected' => $user_permissions->where('name', $permission->name)->count() > 0 ? true : false
            ];
        };
        $keys = array_column($permissions, 'text');
        array_multisort($keys, SORT_DESC, $permissions);
        $user->permissions = $permissions;

        $all_locations = SupportLocation::all()->unique('support_location_desc');
        $user_locations = AllowedLocation::where('user_id', $user->id)->get();

        $locations = [];
        foreach ($all_locations as $index => $location) {
            $locations[] = [
                'id' => $index,
                'text' => $location->support_location_desc,
                'selected' => $user_locations->where('name', $location->support_location_desc)->count() > 0 ? true : false
            ];
        };
        $keys = array_column($locations, 'text');
        array_multisort($keys, SORT_DESC, $locations);
        $user->locations = $locations;

        return $user;
    }
    /**
     * 
     * Add user to survey
     * 
     * @param \Illuminate\Http\Request
     * @return mixed
     */
    public function addUser(Request $request)
    {

        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|unique:users,email',
            'password' => 'required',
        ]);

        $data = $request->all();

        $permissions = isset($data['permissions']) ? $data['permissions'] : [];
        $departments = isset($data['departments']) ? $data['departments'] : [];
        $locations   = isset($data['locations']) ? $data['locations'] : [];
        
        if (isset($data['page']) && $data['page'] == 'all_users') {
            $data['password'] = Hash::make($data['password']);
            $user = User::create($data);
        } else {
            $survey = Survey::find($data['survey_id']);
    
            $this->validateUser($survey);
            $data['password'] = Hash::make($data['password']);
            $user = User::create($data);
            $this->updatePermissions($user, $survey, $permissions);
            $this->updateDepartments($user, $survey, $departments);
            $this->updateLocations($user, $survey, $locations);
    
            // add survey to users allowed surveys
            $user->accountSurveys()->save(new Account(['survey_id' => $survey->survey_id]));
    
            $user->save();
        }

        $request->merge(['user_id' => $user->id]);
        return $this->addToSurvey($request);
    }
    /**
     * 
     * Remove user
     * 
     * @param \App\Models\User
     * @return \App\Models\User
     */
    public function removeUser(User $user)
    {
        // $survey = null;
        // if(Auth::check())
        //     $survey = Survey::find(Auth::user()->survey_id);
        // else
        //     return [];

        // $this->validateUser($survey);
        $user->delete();
        return $user; //still return the existing user so we can update the view if needed
    }
    /**
     * 
     * Update the user
     * 
     * @param \Illuminate\Http\Request
     * @param \App\Models\User
     * @return \App\Models\User
     */
    public function updateUser(Request $request, User $user)
    {

        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'email' => 'required',
        ]);

        $data = $request->all();

        $permissions = isset($data['permissions']) ? $data['permissions'] : [];
        $departments = isset($data['departments']) ? $data['departments'] : [];
        $locations = isset($data['locations']) ? $data['locations'] : [];

        $survey = Survey::find($data['survey_id']);

        $this->validateUser($survey);

        $this->updatePermissions($user, $survey, $permissions);
        $this->updateDepartments($user, $survey, $departments);
        $this->updateLocations($user, $survey, $locations);

        $data['password'] = $data['password'] ? Hash::make($data['password']) : $user->password;
        $user = $user->update($data);
        return $user;
    }
    /**
     * 
     * Add user to the survey
     * 
     * @param \Illuminate\Http\Request
     * @return \App\Models\User
     */
    public function addToSurvey(Request $request)
    {
        $data = $request->all();
        $projects    = isset($data['projects']) ? $data['projects'] : [];

        $user = User::find($data['user_id']);

        if (isset($data['page']) && $data['page'] == 'all_users') {
            foreach ($projects as $survey_id) {
                $survey = Survey::find($survey_id);
                $user->survey_id = $survey_id;
                $user->save();

                $user->accountSurveys()->where('survey_id', $survey_id)->delete();

                $user->accountSurveys()->save(new Account(['survey_id' => $survey_id]));
            }
        } else {
            $survey = Survey::find($data['survey_id']);
    
            $this->validateUser($survey);
            $user->survey_id = $survey->survey_id;
            $user->save();
    
            foreach ($user->permissions as $permission) {
                $permission->value = $survey->survey_id;
                $permission->save();
            }
    
            // remove the mapping if it exists to prevent duplicates
            $user->accountSurveys()->where('survey_id', $survey->survey_id)->delete();
    
            // add the mapping
            $user->accountSurveys()->save(new Account(['survey_id' => $survey->survey_id]));
        }

        $user->load('permissions');
        $user->allowedProjects = $user->allowedProjects;

        return $user;
    }
    /**
     * 
     * Update the permission
     * 
     * @param \App\Models\User $user
     * @param \App\Models\Survey $survey
     * @param \App\Models\Permission $permissions
     * @return void
     */
    private function updatePermissions($user, $survey, $permissions)
    {
        Permission::where('user_id', $user->id)->delete();
        foreach ($permissions as $perm) {
            // Permission::create(['name' => $perm, 'user_id' => $user->id]);
            $permission = new Permission();
            $permission->name = $perm;
            $permission->user_id = $user->id;
            $permission->value = $survey->survey_id; //FIXME: Not exactly sure where there is a reference to the survey on the users table and the permissions table, what is the difference and what is the purpose
            $permission->save();
        }
        $user->load('permissions');
    }
    /**
     * 
     * Update the departments
     * 
     * @param \App\Models\User $user
     * @param \App\Models\Survey $survey
     * @param \App\Models\Department $departments
     * @return void
     */
    private function updateDepartments($user, $survey, $departments)
    {
        Department::where('user_id', $user->id)->delete();
        $allowed_departments = [];
        foreach ($departments as $department) {
            $allowed_departments[] = new Department([
                'user_id' => $user->id,
                'name' => $department,
                'survey_id' => $survey->survey_id
            ]);
        }

        $user->departments()->saveMany($allowed_departments);

        $user->refresh(); // load the newly save departments onto the User model
    }
    /**
     * 
     * Update the locations
     * 
     * @param \App\Models\User $user
     * @param \App\Models\Survey $survey
     * @param \App\Models\Location $location
     * @return void
     */
    private function updateLocations($user, $survey, $locations)
    {
        AllowedLocation::where('user_id', $user->id)->delete();
        $allowed_locations = [];
        foreach ($locations as $location) {
            $allowed_locations[] = new AllowedLocation([
                'user_id' => $user->id,
                'name' => $location,
                'survey_id' => $survey->survey_id
            ]);
        }

        $user->locations()->saveMany($allowed_locations);

        $user->refresh(); // load the newly save departments onto the User model
    }
    /**
     * 
     * Return all users view
     * 
     * @param \App\Models\Survey
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function allUsers(Survey $survey, Request $request)
    {
        $inactive_users = User::onlyTrashed()->paginate(10);
        $active_users = User::paginate(10);

        return view('users.all_users', compact('inactive_users', 'active_users'));
    }
    /**
     * 
     * Deactivate the user
     * 
     * @param \Illuminate\Http\Request
     * @return \App\Models\User
     */
    public function deactivateUser(Request $request)
    {
        if (!\Auth::check() || !\Auth::user()->is_admin)
            return [
                'error' => 'You do not have the required permissions to performs this action'
            ];


        $user = User::find($request->user_id);
        $user->delete();
        $user->allowedProjects = $user->allowedProjects;
        return $user;
    }
    /**
     * 
     * Activate the user
     * 
     * @param \Illuminate\Http\Request
     * @return \App\Models\User
     */
    public function activateUser(Request $request)
    {
        if (!\Auth::check() || !\Auth::user()->is_admin)
            return [
                'error' => 'You do not have the required permissions to performs this action'
            ];

        $user = User::withTrashed()->find($request->user_id);
        $user->restore();
        $user->allowedProjects = $user->allowedProjects;
        return $user;
    }
    /**
     * 
     * Force delete the user
     * 
     * @param \Illuminate\Http\Request
     * @return \App\Models\User
     */
    public function forceDelete(Request $request)
    {
        if (!\Auth::check() || !\Auth::user()->is_admin)
            return [
                'error' => 'You do not have the required permissions to performs this action'
            ];

        $user = User::withTrashed()->find($request->user_id);
        $user->forceDelete();
        return $user;
    }
}
