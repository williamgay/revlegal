<html lang="en">

<head>
    <!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/telwind.output.css')}}"> -->
    <link rel="stylesheet" href="{{ asset('css/additional-styling.css') }}">
</head>

<body>
    <div class="md:px-24 sm:px-8 lg:px-36">

        <div class="my-10 flex justify-between items-end">
            <div>
                <h3 class="font-black dark:text-white font-bold text-2xl">Welcome {{request()->user()->first_name}}. <br>
                    <h4 class="text-black dark:text-white font-bold text-lg"> Please select a project to view your results and analysis.</h4>
                </h3>
            </div>
            @if (\Auth::user()->is_admin)
            <a class="btn btn-revelation-primary" href="{{ route('projects.index') }}">Manage Projects</a>                
            @endif
        </div>
        <hr>
        <div class="my-10" >
            @foreach($surveys as $survey)
            <div class="py-3 shadow-lg rounded-md my-2 flex items-center">
                <a class="px-3 font-bold" href="{{ route('survey', $survey->survey_id) }}">{{$survey->survey_name}}</a></p>
            </div>
            
            @endforeach
        </div>
    </div>

</body>

</html>