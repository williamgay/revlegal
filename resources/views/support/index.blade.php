<x-app-layout>
    <x-slot name="header">

    </x-slot>
    <link rel="stylesheet" href="{{ asset('css/additional-styling.css') }}">
    <div class="mx-20 my-2 flex justify-center">
        <div style="min-width: 1024px;">
            <div class="flex justify-between items-center mt-4 mb-3 border-b">
                <h3 class="text-survey font-bold text-lg mb-0">Create a Support ticket</h3>
            </div>
            <div class="emailFormArea">
                @if (isset($data['sent']) && $data['sent'] == 1)
                    <p class="alert alert-success">Message sent. We will get back to you soon</p>
                @endif                
                @if (isset($data['error']) && $data['error'] == 1)
                    <p class="alert alert-success">Please input a validated email</p>
                @endif                
                <form id="emailForm" method="POST" action="{{ route('support.contact') }}">
                    @csrf
                    <div class="form-group flex items-center justify-between">
                        <label for="email" class="col-4"><span>Contact E-mail:</span> </label>
                        <input type="email" class="form-control col-8" placeholder="Contact Email" id="email" name="email">
                    </div>
                    <div class="form-group flex items-center justify-between">
                        <label for="phone" class="col-4"><span>Contact Phone:</span> </label>
                        <input type="text" class="form-control col-8" placeholder="Contact Phone" id="phone" name="phone">
                    </div>
                    <div class="form-group flex">
                        <label for="message" class="col-4">
                            <span>Message:</span>
                        </label>
                        <textarea class="form-control" name="message" id="message" cols="30" rows="7"></textarea>
                    </div>
                    <div class="flex items-center mt-2">
                        <div class="col-4">
                            <button type="submit" class="btn btn-revelation-primary">Send</button>
                        </div>
                        <div class="col-8 text-center">
                            You may also contact us directly at +1 312.720.6145
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            setTimeout(() => {
                $('.alert').slideUp();
            }, 2000);
        });
    </script>
</x-app-layout>