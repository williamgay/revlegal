@extends('layouts.reports')
@section('content')
    <div class="flex justify-between items-center border-b">
        <h3 class="text-survey font-bold py-4 px-8 m-0 text-lg">Send Participant Invitation Letters</h3>
    </div>
    <div class="p-4">
        <p><span id="receiver_num">{{ $data['resp_num'] }}</span> Selected to receive invitations</p>
        <div class="emailFormArea">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="receiverType" id="all_participant" value="0" checked>
                <label class="form-check-label" for="all_participant">
                  <strong>Send to all participants</strong>
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="receiverType" id="new_participant" value="1">
                <label class="form-check-label" for="new_participant">
                  <strong>Send only to new participants</strong>
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="receiverType" id="non_responders" value="2">
                <label class="form-check-label" for="non_responders">
                  <strong>Send only to non-responders</strong>
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="receiverType" id="partial_completed_participants" value="3">
                <label class="form-check-label" for="partial_completed_participants">
                  <strong>Send only to partial completed participants</strong>
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="receiverType" id="selected_participants" value="4">
                <label class="form-check-label" for="selected_participants">
                  <strong>Send only to selected participants</strong>
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="receiverType" id="test_email" value="5">
                <label class="form-check-label" for="test_email">
                  <strong>Send test email</strong>
                </label>
            </div>
            <div class="form-group selectedRespArea">
                <label for="selectedResp"><strong>Enter emails, comma-separated</strong></label>
                <textarea class="form-control" name="selectedResp" id="selectedResp" cols="30" rows="5" style="width: unset;"></textarea>
            </div>
            <p class="alert alert-success" style="display: none;">Emails sent successfully!</p>
            <p class="alert alert-danger" style="display: none;">Error! Please try again later.</p>
            <button id="sendInvitation" class="btn btn-revelation-primary">Send Invitations</button>
        </div>
    </div>
    <script>
        var survey_id = {{ $data['survey']->survey_id }};
        var receiverType = 0;
        $('.form-check-input').click(function () {
            receiverType = $(this).val();

            if (receiverType == '4') {
                $('.selectedRespArea').slideDown();
            } else {
                $('.selectedRespArea').slideUp();
            }

            if (receiverType < 4) {
                $.ajax({
                    url: "{{ route('invitations/get_participants_num') }}",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "survey_id": survey_id,
                        "receiverType": receiverType
                    },
                    dataType: "json",
                    beforeSend: function () {

                    },
                    success: function (res) {
                        $('#receiver_num').html(res.resp_num);
                    },
                    error: function(request, error) {
                        alert("Request: " + JSON.stringify(request));
                    }
                });
            }
        });

        $('#sendInvitation').click(function () {
            let receiverList = $('#selectedResp').val();

            $.ajax({
                url: "{{ route('invitations/send_emails') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "survey_id": survey_id,
                    "receiverType": receiverType,
                    "receiverList": receiverList
                },
                dataType: "json",
                beforeSend: function () {

                },
                success: function (res) {
                    if (res.status == 200) {
                        $('.alert-success').slideDown();
                    } else {
                        $('.alert-danger').slideDown();
                    }
                    setTimeout(() => {
                        $('.alert').slideUp();
                    }, 3500);
                },
                error: function (request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        });
    </script>
@endsection
