@extends('layouts.reports')
@section('content')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-table.min.css') }}">
    <script src="{{ asset('js/bootstrap-table.min.js') }}"></script>
    <div class="container-fluid">
        <div class="flex justify-between items-center border-b">
            <h3 class="text-survey font-bold p-4 m-0 text-lg">Crosstab Report / {{ $survey->survey_name }}</h3>
            <div>
                @if (\Auth::check() && \Auth::user()->hasPermission('surveyExport', $survey))
                    <button class="btn btn-revelation-primary" id="excelBtn">Export to Excel</button>                    
                @endif
                @if (\Auth::check() && \Auth::user()->hasPermission('surveyPrint', $survey))
                    <button class="btn mr-4 ml-2" style="background-color: #0095C2; color: white;" id="pdfPrint">Download PDF</button>                
                @endif
            </div>
        </div>
        <div id="individualContent" class="container-fluid">
            <div class="first_part border-bottom">
                <div class="row">
                    <div class="col-12 px-8 text-gray-500 font-bold mt-2">
                        <div class="lead_in">
                            <h5 class="font-bold text-gray-500">{{$data['survey']->survey_name}}</h5>
                            <h5 class="text-lg">Crosstab Breakdown</h5>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-12 col-lg-2 mt-2">
                            <strong>Classification</strong>
                            <select name="taxonomy" id="taxonomy" class="form-control" >
                                <option value="">(All)</option>
                                @foreach($data['taxonomy'] as $tax)
                                    <option value="{{ $tax->question_desc }}">{{ $tax->question_desc }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-lg-2 mt-2">
                            <strong>Metric</strong>
                            <select class="form-control" id="metric">
                                @foreach(App\Http\Controllers\CrosstabReportController::getMetricMap() as $metric => $title)
                                    <option value="{{ $metric }}">{{ $title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-lg-2 mt-2">
                            <strong>Veritcal Breakdown</strong>
                            <select class="form-control" id="vertical_breakdown">
                                <option value="0">Support or Legal</option>
                                <option value="1" selected>Classification</option>
                                <option value="2">Substantive Area</option>
                                <option value="3">Category</option>
                                <option value="4">Process</option>
                            </select>
                        </div>
                        <div class="col-12 col-lg-2 mt-2">
                            <strong>Horizontal Breakdown</strong>
                            <select class="form-control" id="horizontal_breakdown">
                                <option value="group">Group</option>
                                <option value="position">Position</option>
                                <option value="department">Department</option>
                                <option value="category">Category</option>
                                <option value="location" selected>Location</option>
                            </select>
                        </div>
                        <div class="col-12 col-lg-2 mt-2">
                            <strong>Location</strong>
                            <select class="form-control" id="location">
                                <option value="">(All)</option>
                                @foreach($data['location'] as $location)
                                    <option value="{{ $location }}">{{ $location }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-lg-2 mt-2 text-center">
                            <img src="{{asset('imgs/logo-new-small_rev.png')}}" alt="" style="height: 70px;" class="d-inline-block">
                        </div>
                    </div>
                </div>
            </div>

            <div class="second_part container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="reportBreakdownChart" class="table table-sm text-sm">
                                <thead></thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <div class="modal fade" tabindex="-1" role="dialog" id="generateExcelModal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body flex items-center justify-center" style="height: 150px;">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating Excel file ...
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-revelation-primary disabled" href="javascript:void(0);">Download</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="copyright_div" class="flex justify-begin items-center" style="width:100%;background-color:white;padding:10px;font-size:11px;">
            <div>
                <img src="{{asset('imgs/logo-new-small_rev.png')}}" style="height:60px" alt="">
            </div>
            <div>
                <a href="http://www.revelationlegal.com">http://www.revelationlegal.com</a> <br>
                <span>&copy; ofPartner LLC, All Rights Reserved. Report Generated @php echo date('m/d/Y') @endphp</span>
            </div>
        </div>
    </div>

    <script>
        const survey_id = <?= $data["survey"]->survey_id ?>;
        const survey = @JSON($data['survey']);
        const $table = $('#report_breakdown_table');

        $(init);

        function init(){
            $('#taxonomy').trigger('change'); // trigger a change event on the selectors for initial page load so the user doesn't have to
        }

        $('#taxonomy, #metric, #vertical_breakdown, #horizontal_breakdown, #location').on('change', function(){
            getFilteredData(
                $('#taxonomy').val(),
                $('#metric').val(),
                $('#vertical_breakdown').val(),
                $('#horizontal_breakdown').val(),
                $('#location').val(),
                survey.survey_id
            );
        });

        $('#pdfPrint').on('click', function(){
            const hideElements = ['#desktop_sidebar', '.first_part', '#pdfPrint', 'header > div > ul'];

            $.each(hideElements, function(_, el){ $(el).hide(); });

            window.print();

            $.each(hideElements, function(_, el){ $(el).show(); });
        });

        function getFilteredData(taxonomy, metric, vertical_breakdown, horizontal_breakdown, location, survey_id)
        {
            showLoader();
            $.post('/reports/crosstab/individual', {
                taxonomy,
                metric,
                vertical_breakdown,
                horizontal_breakdown,
                location,
                survey_id,
            })
            .done(function(data){
                hideLoader();
                loadResults(data);
            })
            .catch(function(data){
                hideLoader();
                alert('An error occured while fetching report data');
            });
        }

        function loadResults(results)
        {

            const { breakdown, headings, max, min, average } = results;

            const aboveAverage = average + (average * .75);


            $('#reportBreakdownChart > thead').empty();
            $.each(headings, function(_, heading){
                $("#reportBreakdownChart > thead").append(`<th class="text-right">${heading == 'EMPTY' ? '' : heading}</th>`);
            });

            $('#reportBreakdownChart > tbody').empty();
            $.each(breakdown, function(question, columns){

                const row = $('<tr></tr>');

                row.append(`<td>${question}</td>`);

                $.each(headings, function(_, column){
                    if (column != 'EMPTY') {

                        const answer = columns[column] && columns[column] >= 1
                            ? Math.floor(columns[column])
                            : 0;

                        const percentage = (answer / max);

                        const bg = getBackground(percentage, aboveAverage);

                        row.append(`<td class="text-right border" style="color: #373a3c; background-color: ${bg}; ">${formatNumber(answer)}</td>`);
                    }

                });

                $('#reportBreakdownChart > tbody').append(row);
            });
        }



        const supportColor = "#82BD5E";
        const legalColor = "#367BC1";
        const highColor = "#f6022f";
        function getBackground(percentage, max) {

            // let color = supportColor; // initialize color
            // if(percentage >= max)
            //     color = highColor;
            // else
                color = legalColor;

            color_red = 130 + 125 * percentage;
            color_green = 189 - 189 * percentage;

            // const bg = 'rgba(' + parseInt(color.slice(-6,-4),16)
            //                     + ',' + parseInt(color.slice(-4,-2),16)
            //                     + ',' + parseInt(color.slice(-2),16)
            //                     +',' + (percentage <= 0 ? 0.01 : percentage) + ')';
            
            const bg = `rgba(${color_red}, ${color_green}, 94, 0.9)`;

            return bg;
        }

        function formatNumber(number) {
            return number.toLocaleString('en-US');
        }

        // Handle the event of excel button click
        $('#excelBtn').click(function () {
            let headers = $("#reportBreakdownChart th").map(function() {
                return this.innerHTML;
            }).get();

            let rows = $("#reportBreakdownChart tbody tr").map(function () {
                return [$("td",this).map(function() { 
                    return this.innerHTML;     
                }).get()];
            }).get();

            $.ajax({
                url: '{{ route('individual-crosstabreport.export-excel') }}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "survey_id": survey_id,
                    "headers": JSON.stringify(headers),
                    "rows": JSON.stringify(rows),
                },
                dataType: 'json',
                beforeSend: function () {
                    $('#generateExcelModal').modal('show');
                },
                success: function (res) {
                    $('#generateExcelModal .modal-body').html('Generated an Excel file');
                    $('#generateExcelModal .btn').attr('href', res.url);
                    $('#generateExcelModal .btn').attr('download', res.filename);
                    $('#generateExcelModal .btn').removeClass('disabled');
                },
                error: function(request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        });        

        $('#generateExcelModal').on('hidden.bs.modal', function () {
            $('#generateExcelModal').modal('hide');
            $('#generateExcelModal .modal-body').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating Excel file...`);
            $('#generateExcelModal .btn').attr('href', 'javascript:void(0);');
            $('#generateExcelModal .btn').addClass('disabled');
        });

        $('#generateExcelModal .btn').click(function () {
            $('#generateExcelModal').modal('hide');
        });

    </script>

    @include('partials.loader')
@endsection
