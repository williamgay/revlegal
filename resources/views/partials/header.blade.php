<header class="z-10 bg-black dark:bg-gray-800" id="header">

    <div class="flex items-center justify-between px-6 mx-auto text-purple-600 dark:text-purple-300">
        <!-- Mobile hamburger -->
        <button style="color: #337ab7;" class="p-1 mr-5 -ml-1 rounded-md md:hidden focus:outline-none focus:shadow-outline-purple" @click="toggleSideMenu" aria-label="Menu">
            <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                <path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path>
            </svg>
        </button>
        <img class="w-44 mr-auto" src="{{asset('imgs/logo-new-small-rev.png')}}">
        <ul class="menu border-b md:border-none flex list-reset mx-10 w-full md:w-auto" style="margin-bottom:0;">
            <li class="border-t md:border-none">
                <a class="block md:inline-block px-4 py-3 text-white hover:text-gray-500" href="{{route('projects')}}"><strong>Projects</strong></a>
            </li>
            @if(\Auth::check() && \Auth::user()->is_admin)
                <li class="border-t md:border-none">
                    <a class="block md:inline-block px-4 py-3 text-white hover:text-gray-800" href="{{ route('users.all') }}">
                        <strong> All Users </strong>
                    </a>
                </li>
            @endif
            <li class="border-t md:border-none">
                <a class="block md:inline-block px-4 py-3 text-white hover:text-gray-800"
                   href="{{ route('profile.show') }}"><strong>My Account</strong></a>

            </li>
            <li class="border-t md:border-none">
                <a class="block md:inline-block px-4 py-3 text-white hover:text-gray-800" href="{{ route('support.index') }}"><strong>Support</strong></a>
            </li>
            <li class="border-t md:border-none">
                <a class="block md:inline-block px-4 py-3 text-white hover:text-gray-800" href="{{ route('logout') }}"><strong>Log Out({{request()->user()->first_name}})</strong></a>
            </li>
        </ul>
    </div>
</header>
