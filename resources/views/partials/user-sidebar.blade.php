<!-- Desktop sidebar -->
<aside class="z-20 hidden w-64 overflow-y-auto shadow-md bg-white dark:bg-gray-800 md:block flex-shrink-0" id="desktop_sidebar">
    <div class="py-4 anchor text-gray-500 dark:text-gray-400" style="height:100%;">

        @if (\Auth::check() && \Auth::user()->hasPermission('survey', $survey))
        <ul class="mt-2">
            <li class="relative px-6 py-3">
                <!-- <span class="absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> -->
                <a class="@if(Route::currentRouteName() == 'projects') active @endif inline-flex text-gray-500 items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100" href="{{route('projects')}}">
                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M9 21H5c-1.1 0-2-.9-2-2V5c0-1.1.9-2 2-2h4c1.1 0 2 .9 2 2v14c0 1.1-.9 2-2 2zm6 0h4c1.1 0 2-.9 2-2v-5c0-1.1-.9-2-2-2h-4c-1.1 0-2 .9-2 2v5c0 1.1.9 2 2 2zm6-13V5c0-1.1-.9-2-2-2h-4c-1.1 0-2 .9-2 2v3c0 1.1.9 2 2 2h4c1.1 0 2-.9 2-2z" fill="currentColor"/z></svg>
                    <span class="ml-4">Projects</span>
                </a>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyProfile', $survey))
        <ul class="mt-2">
            <li class="relative px-6 py-3">
                <!-- <span class="active absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> -->
                <a class="@if(Route::currentRouteName() == 'survey') active @endif inline-flex text-gray-500 items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100"  href="{{route('survey', $data['survey']->survey_id)}}">
                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><g fill="none"><path d="M17.354 2.646a2.621 2.621 0 0 0-3.707 0l-5.5 5.5a.5.5 0 0 0-.132.232l-1 4a.5.5 0 0 0 .606.607l4-1a.5.5 0 0 0 .233-.132l5.5-5.5a2.621 2.621 0 0 0 0-3.707z" fill="currentColor"/><path d="M15.944 9.177a6 6 0 1 1-5.121-5.121l.854-.854a7 7 0 1 0 5.121 5.121l-.854.854z" fill="currentColor"/></g></svg>
                    <span class="ml-4">Status</span>
                </a>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyUsers', $survey))
        <ul class="mt-2">
            <li class="relative px-6 py-3">
                <!-- <span class="active absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> -->
                <a class="@if(Route::currentRouteName() == 'survey_users') active @endif inline-flex text-gray-500 items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100"  href="{{route('survey_users', $data['survey']->survey_id)}}">
                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 36 36"><path class="clr-i-solid clr-i-solid-path-1" d="M12 16.14h-.87a8.67 8.67 0 0 0-6.43 2.52l-.24.28v8.28h4.08v-4.7l.55-.62l.25-.29a11 11 0 0 1 4.71-2.86A6.59 6.59 0 0 1 12 16.14z" fill="currentColor"/><path class="clr-i-solid clr-i-solid-path-2" d="M31.34 18.63a8.67 8.67 0 0 0-6.43-2.52a10.47 10.47 0 0 0-1.09.06a6.59 6.59 0 0 1-2 2.45a10.91 10.91 0 0 1 5 3l.25.28l.54.62v4.71h3.94v-8.32z" fill="currentColor"/><path class="clr-i-solid clr-i-solid-path-3" d="M11.1 14.19h.31a6.45 6.45 0 0 1 3.11-6.29a4.09 4.09 0 1 0-3.42 6.33z" fill="currentColor"/><path class="clr-i-solid clr-i-solid-path-4" d="M24.43 13.44a6.54 6.54 0 0 1 0 .69a4.09 4.09 0 0 0 .58.05h.19A4.09 4.09 0 1 0 21.47 8a6.53 6.53 0 0 1 2.96 5.44z" fill="currentColor"/><circle class="clr-i-solid clr-i-solid-path-5" cx="17.87" cy="13.45" r="4.47" fill="currentColor"/><path class="clr-i-solid clr-i-solid-path-6" d="M18.11 20.3A9.69 9.69 0 0 0 11 23l-.25.28v6.33a1.57 1.57 0 0 0 1.6 1.54h11.49a1.57 1.57 0 0 0 1.6-1.54V23.3l-.24-.3a9.58 9.58 0 0 0-7.09-2.7z" fill="currentColor"/></svg>
                    <span class="ml-4">Project Users</span>
                </a>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyTaxonomy', $survey))
        <ul class="mt-2">
            <li class="relative px-6 py-3">
                <!-- <span class="active absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> -->
                <a class="@if(Route::currentRouteName() == 'taxonomy.index') active @endif inline-flex text-gray-500 items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100"  href="{{route('taxonomy.index', $data['survey']->survey_id)}}">
                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="24" height="24" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="currentColor"><path d="M11.34 9.71h.71l2.67-2.67v-.71L13.38 5h-.7l-1.82 1.81h-5V5.56l1.86-1.85V3l-2-2H5L1 5v.71l2 2h.71l1.14-1.15v5.79l.5.5H10v.52l1.33 1.34h.71l2.67-2.67v-.71L13.37 10h-.7l-1.86 1.85h-5v-4H10v.48l1.34 1.38zm1.69-3.65l.63.63l-2 2l-.63-.63l2-2zm0 5l.63.63l-2 2l-.63-.63l2-2zM3.35 6.65l-1.29-1.3l3.29-3.29l1.3 1.29l-3.3 3.3z"/></g></svg>
                    <span class="ml-4">Taxonomy</span>
                </a>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveySettings', $survey))
        <ul>
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center focus:outline-none justify-between w-full text-sm font-semibold duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu6" aria-haspopup="true">
                    <span class="@if(Request::is('settings/*')) active @endif inline-flex items-center">
                       <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none"><path d="M21.031 2.97a3.579 3.579 0 0 1 0 5.06l-2.992 2.992a6.565 6.565 0 0 0-2.358.236L17.94 9L15.001 6.06L5.063 16a.75.75 0 0 0-.193.332l-1.05 3.85l3.85-1.05a.75.75 0 0 0 .332-.193l3.257-3.257a6.501 6.501 0 0 0-.236 2.357l-1.96 1.96a2.25 2.25 0 0 1-1 .58l-5.115 1.395a.75.75 0 0 1-.92-.92l1.394-5.116a2.25 2.25 0 0 1 .58-1L15.97 2.97a3.578 3.578 0 0 1 5.061 0zm-4 1.06l-.97.97L19 7.94l.97-.97a2.078 2.078 0 1 0-2.939-2.94zm-2.752 9.945a2 2 0 0 1-1.441 2.497l-.584.144a5.729 5.729 0 0 0 .006 1.807l.54.13a2 2 0 0 1 1.45 2.51l-.187.632c.44.386.94.699 1.484.921l.494-.518a2 2 0 0 1 2.899 0l.498.525a5.28 5.28 0 0 0 1.483-.913l-.198-.686a2 2 0 0 1 1.442-2.496l.583-.144a5.729 5.729 0 0 0-.006-1.808l-.54-.13a2 2 0 0 1-1.45-2.51l.187-.63a5.28 5.28 0 0 0-1.484-.923l-.493.519a2 2 0 0 1-2.9 0l-.498-.525c-.544.22-1.044.53-1.483.912l.198.686zM17.501 19c-.8 0-1.45-.672-1.45-1.5c0-.829.65-1.5 1.45-1.5c.8 0 1.45.671 1.45 1.5c0 .828-.65 1.5-1.45 1.5z" fill="currentColor"/></g></svg>
                        <span class="ml-4">Settings</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen6">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">

                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('settings-settings',$data['survey']->survey_id)}}">Settings</a>
                        </li>
                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('settings-locations',$data['survey']->survey_id)}}">
                                Locations
                            </a>
                        </li>
                    </ul>
                </template>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyRespondents', $survey))
        <ul class="mt-2">
            <li class="relative px-6 py-3">
                <!-- <span class="active absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> -->
                <a class="@if(Route::currentRouteName() == 'Respondents') active @endif inline-flex text-gray-500 items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100"  href="{{ route('Respondents', $data['survey']->survey_id) }}">
                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 256 256"><path d="M172 120a44 44 0 1 1-44-44a44 44 0 0 1 44 44zm60-64v144a16.018 16.018 0 0 1-16 16H40a16.018 16.018 0 0 1-16-16V56a16.018 16.018 0 0 1 16-16h176a16.018 16.018 0 0 1 16 16zm-15.99 144L216 56H40v144h14.678a80.03 80.03 0 0 1 32.054-36.508a59.837 59.837 0 0 0 82.536 0A80.03 80.03 0 0 1 201.322 200H216z" fill="currentColor"/></svg>
                    <span class="ml-4">Participants</span>
                </a>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyInvitations', $survey))
        <ul>
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center focus:outline-none justify-between w-full text-sm font-semibold duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu5" aria-haspopup="true">
                    <span class="@if(Request::is('invitations/*')) active @endif inline-flex items-center">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path opacity=".3" d="M19 5H5v2h14z" fill="currentColor"/><path d="M5 21h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19a2 2 0 0 0 2 2zM5 7V5h14v2H5zm0 2h14v10H5V9zm7 3h5v5h-5z" fill="currentColor"/></svg>
                        <span class="ml-4">Invitations</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen5">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">

                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('invitations-settings',$data['survey']->survey_id)}}">Settings</a>
                        </li>
                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('invitations-send',$data['survey']->survey_id)}}">
                                Send Invitations
                            </a>
                        </li>
                    </ul>
                </template>
            </li>
        </ul>
        @endif
        <ul>
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyReports', $survey))
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center focus:outline-none justify-between w-full text-sm font-semibold duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu" aria-haspopup="true">
                    <span class="@if(Request::is('reports/general/*')) active @endif inline-flex items-center">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 32 32"><path d="M10 18h8v2h-8z" fill="currentColor"/><path d="M10 13h12v2H10z" fill="currentColor"/><path d="M10 23h5v2h-5z" fill="currentColor"/><path d="M25 5h-3V4a2 2 0 0 0-2-2h-8a2 2 0 0 0-2 2v1H7a2 2 0 0 0-2 2v21a2 2 0 0 0 2 2h18a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2zM12 4h8v4h-8zm13 24H7V7h3v3h12V7h3z" fill="currentColor"/></svg>
                        <span class="ml-4">Reports</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">

                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('individual-report',$data['survey']->survey_id)}}">Individual</a>
                        </li>
                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('demographic_report',$data['survey']->survey_id)}}">
                            Demographic
                            </a>
                        </li>
                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('compilation-report', $data['survey']->survey_id) }}">
                            Compilation
                            </a>
                        </li>
                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('individual-crosstabreport', $data['survey']->survey_id) }}">Crosstab</a>
                        </li>
                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('validation-report', $data['survey']->survey_id) }}">Validation Reports (Beta)</a>
                        </li>
                    </ul>
                </template>
            </li>
            @endif
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyNoCompReport', $survey))
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center focus:outline-none justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu2" aria-haspopup="true">
                    <span class="@if(Request::is('reports/nc/*')) active @endif inline-flex items-center">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 32 32"><path d="M15 20h2v4h-2z" fill="currentColor"/><path d="M20 18h2v6h-2z" fill="currentColor"/><path d="M10 14h2v10h-2z" fill="currentColor"/><path d="M25 5h-3V4a2 2 0 0 0-2-2h-8a2 2 0 0 0-2 2v1H7a2 2 0 0 0-2 2v21a2 2 0 0 0 2 2h18a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2zM12 4h8v4h-8zm13 24H7V7h3v3h12V7h3z" fill="currentColor"/></svg>
                        <span class="ml-4">NC Reports</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen2">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('individual-ncreport',$data['survey']->survey_id)}}">
                            Individual
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('nc_demographic_report', $data['survey']->survey_id) }}">
                                Demographic
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('compilation-ncreport', $data['survey']->survey_id) }}">
                            Compilation
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('individual-crosstabreport', $data['survey']->survey_id) }}">Crosstab</a>
                        </li>
                    </ul>
                </template>
            </li>
            @endif
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyAnalysis', $survey))
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center focus:outline-none justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu3" aria-haspopup="true">
                    <span class="@if(Request::is('analysis/*')) active @endif inline-flex items-center">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 32 32"><path d="M8 10h8v2H8z" fill="currentColor"/><path d="M8 6h12v2H8z" fill="currentColor"/><path d="M8 2h12v2H8z" fill="currentColor"/><path d="M4.711 28l5.631-9.996l7.434 6.49a2 2 0 0 0 3.084-.534l6.97-10.403l-1.661-1.114l-7 10.448l-.07.103l-7.435-6.49a2.003 2.003 0 0 0-3.08.53L4 25.183V2H2v26a2.002 2.002 0 0 0 2 2h26v-2z" fill="currentColor"/></svg>
                        <span class="ml-4">Analysis</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen3">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('participant-analysis', [$data['survey']->survey_id]) }}">Participant</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('ataglance-analysis', [$data['survey']->survey_id]) }}">
                            At A Glance
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('comparative-glance-analysis', [$data['survey']->survey_id]) }}">
                            Comparative Glance (Beta)
                            </a>
                        </li>
                    </ul>
                </template>
            </li>
            @endif
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyRealEstate', $survey))
            <li class="relative px-6 py-3">
                <button class="@if(Request::is('realestate/*')) active @endif inline-flex items-center focus:outline-none justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 focus:outline-none" @click="togglePagesMenu4" aria-haspopup="true">
                    <span class="inline-flex items-center">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M1 22h4V11H1v11zm19-5h-7l-2.09-.73l.33-.94L13 16h2.82c.65 0 1.18-.53 1.18-1.18c0-.49-.31-.93-.77-1.11L8.97 11H7v9.02L14 22l8-3c-.01-1.1-.89-2-2-2zM14 1.5l-7 5V9h2l8.14 3.26C18.26 12.71 19 13.79 19 15h2V6.5l-7-5zm-.5 8.5h-1V9h1v1zm0-2h-1V7h1v1zm2 2h-1V9h1v1zm0-2h-1V7h1v1z" fill="currentColor"/></svg>
                        <span class="ml-4">Real Estate</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen4">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.location_rsf_rates', [$data['survey']->survey_id]) }}">Location RSF Rates</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.individual-proximity', [$data['survey']->survey_id]) }}">
                            Individual Proximity
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.participant-proximity', [$data['survey']->survey_id]) }}">
                            Participant Proximity
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.activity-by-location', [$data['survey']->survey_id]) }}">Activity by Location</a>                            
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.opportunity-detail', [$data['survey']->survey_id]) }}">Opportunity Detail</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.opportunity-summary', [$data['survey']->survey_id]) }}">Opportunity Summary</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.activity-cost-by-location', [$data['survey']->survey_id]) }}">Activity Cost by Location</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.proximity-by-activity', [$data['survey']->survey_id]) }}">Proximity by Activity(Beta)</a>
                        </li>
                    </ul>
                </template>
            </li>
            @endif
        </ul>
    </div>
</aside>
<!-- Mobile sidebar -->
<!-- Backdrop -->
<div x-show="isSideMenuOpen" x-transition:enter="transition ease-in-out duration-150" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition ease-in-out duration-150" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="fixed inset-0 z-10 flex items-end bg-black bg-opacity-50 sm:items-center sm:justify-center"></div>
<aside class="fixed inset-y-0 z-20 flex-shrink-0 w-64 mt-16 overflow-y-auto shadow-md bg-white dark:bg-gray-800 md:hidden" x-show="isSideMenuOpen" x-transition:enter="transition ease-in-out duration-150" x-transition:enter-start="opacity-0 transform -translate-x-20" x-transition:enter-end="opacity-100" x-transition:leave="transition ease-in-out duration-150" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0 transform -translate-x-20" @click.away="closeSideMenu" @keydown.escape="closeSideMenu">
    <div class="py-4 text-gray-500 dark:text-gray-400">
        <!-- <a class="ml-6 text-lg font-bold text-gray-800 dark:text-gray-200" href="#">
            Windmill
        </a> -->
        @if(\Auth::check() && \Auth::user()->is_admin)
            <ul class="mt-6">
                <li class="relative px-6 py-3">
                    <a class="@if(Route::currentRouteName() == 'users.all') active @endif inline-flex text-gray-500 items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100" href="{{ route('users.all', $data['survey']->survey_id) }}">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 36 36"><path class="clr-i-solid clr-i-solid-path-1" d="M12 16.14h-.87a8.67 8.67 0 0 0-6.43 2.52l-.24.28v8.28h4.08v-4.7l.55-.62l.25-.29a11 11 0 0 1 4.71-2.86A6.59 6.59 0 0 1 12 16.14z" fill="currentColor"/><path class="clr-i-solid clr-i-solid-path-2" d="M31.34 18.63a8.67 8.67 0 0 0-6.43-2.52a10.47 10.47 0 0 0-1.09.06a6.59 6.59 0 0 1-2 2.45a10.91 10.91 0 0 1 5 3l.25.28l.54.62v4.71h3.94v-8.32z" fill="currentColor"/><path class="clr-i-solid clr-i-solid-path-3" d="M11.1 14.19h.31a6.45 6.45 0 0 1 3.11-6.29a4.09 4.09 0 1 0-3.42 6.33z" fill="currentColor"/><path class="clr-i-solid clr-i-solid-path-4" d="M24.43 13.44a6.54 6.54 0 0 1 0 .69a4.09 4.09 0 0 0 .58.05h.19A4.09 4.09 0 1 0 21.47 8a6.53 6.53 0 0 1 2.96 5.44z" fill="currentColor"/><circle class="clr-i-solid clr-i-solid-path-5" cx="17.87" cy="13.45" r="4.47" fill="currentColor"/><path class="clr-i-solid clr-i-solid-path-6" d="M18.11 20.3A9.69 9.69 0 0 0 11 23l-.25.28v6.33a1.57 1.57 0 0 0 1.6 1.54h11.49a1.57 1.57 0 0 0 1.6-1.54V23.3l-.24-.3a9.58 9.58 0 0 0-7.09-2.7z" fill="currentColor"/></svg>
                        <span class="ml-4">All Users</span>
                    </a>
                </li>
            </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('survey', $survey))
        <ul class="mt-6">

            <li class="relative px-6 py-3">
                <!-- <span class="absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> -->
                <a class="inline-flex items-center w-full text-sm font-semibold text-gray-800 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100" href="#">
                    <svg class="w-5 h-5" aria-hidden="true" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>
                    </svg>
                    <span class="ml-4">Projects</span>
                </a>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveySettings', $survey))
        <ul>
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center focus:outline-none justify-between w-full text-sm font-semibold duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu6" aria-haspopup="true">
                    <span class="@if(Request::is('settings/*')) active @endif inline-flex items-center">
                       <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none"><path d="M21.031 2.97a3.579 3.579 0 0 1 0 5.06l-2.992 2.992a6.565 6.565 0 0 0-2.358.236L17.94 9L15.001 6.06L5.063 16a.75.75 0 0 0-.193.332l-1.05 3.85l3.85-1.05a.75.75 0 0 0 .332-.193l3.257-3.257a6.501 6.501 0 0 0-.236 2.357l-1.96 1.96a2.25 2.25 0 0 1-1 .58l-5.115 1.395a.75.75 0 0 1-.92-.92l1.394-5.116a2.25 2.25 0 0 1 .58-1L15.97 2.97a3.578 3.578 0 0 1 5.061 0zm-4 1.06l-.97.97L19 7.94l.97-.97a2.078 2.078 0 1 0-2.939-2.94zm-2.752 9.945a2 2 0 0 1-1.441 2.497l-.584.144a5.729 5.729 0 0 0 .006 1.807l.54.13a2 2 0 0 1 1.45 2.51l-.187.632c.44.386.94.699 1.484.921l.494-.518a2 2 0 0 1 2.899 0l.498.525a5.28 5.28 0 0 0 1.483-.913l-.198-.686a2 2 0 0 1 1.442-2.496l.583-.144a5.729 5.729 0 0 0-.006-1.808l-.54-.13a2 2 0 0 1-1.45-2.51l.187-.63a5.28 5.28 0 0 0-1.484-.923l-.493.519a2 2 0 0 1-2.9 0l-.498-.525c-.544.22-1.044.53-1.483.912l.198.686zM17.501 19c-.8 0-1.45-.672-1.45-1.5c0-.829.65-1.5 1.45-1.5c.8 0 1.45.671 1.45 1.5c0 .828-.65 1.5-1.45 1.5z" fill="currentColor"/></g></svg>
                        <span class="ml-4">Settings</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen6">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">

                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('settings-settings',$data['survey']->survey_id)}}">Settings</a>
                        </li>
                        <li class="px-2 py-1 duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('settings-locations',$data['survey']->survey_id)}}">
                                Locations
                            </a>
                        </li>
                    </ul>
                </template>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyProfile', $survey))
        <ul class="mt-2">
            <li class="relative px-6 py-3">
                <!-- <span class="absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> -->
                <a class="inline-flex items-center w-full text-sm font-semibold text-gray-800 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100" href="{{route('survey', $data['survey']->survey_id)}}">
                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><g fill="none"><path d="M17.354 2.646a2.621 2.621 0 0 0-3.707 0l-5.5 5.5a.5.5 0 0 0-.132.232l-1 4a.5.5 0 0 0 .606.607l4-1a.5.5 0 0 0 .233-.132l5.5-5.5a2.621 2.621 0 0 0 0-3.707z" fill="currentColor"/><path d="M15.944 9.177a6 6 0 1 1-5.121-5.121l.854-.854a7 7 0 1 0 5.121 5.121l-.854.854z" fill="currentColor"/></g></svg>
                    <span class="ml-4">Status</span>
                </a>
            </li>
        </ul>
        @endif
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyInvitations', $survey))
        <ul>
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu5" aria-haspopup="true">
                    <span class="inline-flex items-center">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path opacity=".3" d="M19 5H5v2h14z" fill="currentColor"/><path d="M5 21h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19a2 2 0 0 0 2 2zM5 7V5h14v2H5zm0 2h14v10H5V9zm7 3h5v5h-5z" fill="currentColor"/></svg>
                        <span class="ml-4">Invitations</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen5">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('invitations-settings',$data['survey']->survey_id)}}">Settings</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('invitations-send',$data['survey']->survey_id)}}">
                            Send Invitations
                            </a>
                        </li>
                    </ul>
                </template>
            </li>
        </ul>
        @endif
        <ul>
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyReports', $survey))
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu" aria-haspopup="true">
                    <span class="inline-flex items-center">
                        <svg class="w-5 h-5" aria-hidden="true" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                            <path d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"></path>
                        </svg>
                        <span class="ml-4">Reports</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                        @if (\Auth::check() && \Auth::user()->hasPermission('surveyIndividual', $survey))
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('individual-report',$data['survey']->survey_id)}}">Individual</a>
                        </li>
                        @endif
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{route('demographic_report',$data['survey']->survey_id)}}">
                            Demographic
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="#">
                                Compilation
                            </a>
                        </li>
                        @if (\Auth::check() && \Auth::user()->hasPermission('surveyCrosstab', $survey))
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('individual-crosstabreport', $data['survey']->survey_id) }}">Crosstab</a>
                        </li>
                        @endif
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="#">Validation Reports (Beta)</a>
                        </li>
                    </ul>
                </template>
            </li>
            @endif
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyNoCompReport', $survey))
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu2" aria-haspopup="true">
                    <span class="inline-flex items-center">
                        <svg class="w-5 h-5" aria-hidden="true" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                            <path d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"></path>
                        </svg>
                        <span class="ml-4">NC Reports</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen2">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="#">Demographic</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="#">
                            Individual
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="#">
                            Compilation
                            </a>
                        </li>
                        @if (\Auth::check() && \Auth::user()->hasPermission('surveyCrosstab', $survey))
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('individual-crosstabreport', $data['survey']->survey_id) }}">Crosstab</a>
                        </li>
                        @endif
                    </ul>
                </template>
            </li>
            @endif
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyAnalysis', $survey))
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu3" aria-haspopup="true">
                    <span class="inline-flex items-center">
                        <svg class="w-5 h-5" aria-hidden="true" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                            <path d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"></path>
                        </svg>
                        <span class="ml-4">Analysis</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen3">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('participant-analysis', [$data['survey']->survey_id]) }}">Participant</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('ataglance-analysis', [$data['survey']->survey_id]) }}">
                            At A Glance
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('comparative-glance-analysis', [$data['survey']->survey_id]) }}">
                            Comparative Glance (Beta)
                            </a>
                        </li>
                    </ul>
                </template>
            </li>
            @endif
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyRealEstate', $survey))
            <li class="relative px-6 py-3">
                <button class="inline-flex items-center justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 focus:outline-none" @click="togglePagesMenu4" aria-haspopup="true">
                    <span class="inline-flex items-center">
                        <svg class="w-5 h-5" aria-hidden="true" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                            <path d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"></path>
                        </svg>
                        <span class="ml-4">Real Estate</span>
                    </span>
                    <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                </button>
                <template x-if="isPagesMenuOpen4">
                    <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.location_rsf_rates', [$data['survey']->survey_id]) }}">Location RSF Rates</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.individual-proximity', [$data['survey']->survey_id]) }}">
                            Individual Proximity
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.participant-proximity', [$data['survey']->survey_id]) }}">
                            Participant Proximity
                            </a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.activity-by-location', [$data['survey']->survey_id]) }}">Activity by Location</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.opportunity-detail', [$data['survey']->survey_id]) }}">Opportunity Detail</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.opportunity-summary', [$data['survey']->survey_id]) }}">Opportunity Summary</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.activity-cost-by-location', [$data['survey']->survey_id]) }}">Activity Cost by Location</a>
                        </li>
                        <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                            <a class="w-full" href="{{ route('real-estate.proximity-by-activity', [$data['survey']->survey_id]) }}">Proximity by Activity(Beta)</a>
                        </li>
                    </ul>
                </template>
            </li>
            @endif
        </ul>

    </div>
</aside>

<script>
    $('#desktop_sidebar a').click(function () {
        mask_height = $('body').height();
        $('.loading-mask').css('height', mask_height);
        $('.loading-mask').show();
    });
</script>
