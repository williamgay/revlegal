@extends('layouts.reports')
@section('content')
    <div class="flex justify-between items-center border-b">
        <h3 class="text-survey font-bold py-4 px-8 m-0 text-lg">Proximity by Activity(Beta) / {{ $survey->survey_name }}</h3>
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyPrint', $survey))
            <button class="btn btn-revelation-primary mx-4" id="pdfBtn">Download PDF</button>            
        @endif
    </div>
    <div id="participantProximityContent">
        <div class="first_part" style="background: white;">
            @include('real_estate.partials.proximity-by-activity-filter')
        </div>
        <div class="second_part px-4 pt-4" style="background: white;">
            <div id="proximity_bar" class="flex w-full">
                <div class="high-bar text-center" style="width: {{ $data['rsf_percent_data']['high_percent'] }}%;">
                    <div class="title font-bold">High</div>
                    <div class="value">{{ number_format($data['rsf_percent_data']['high_hours']) }}</div>
                </div>
                <div class="med-bar text-center" style="width: {{ $data['rsf_percent_data']['med_percent'] }}%;">                    
                    <div class="title font-bold">Med</div>
                    <div class="value">{{ number_format($data['rsf_percent_data']['med_hours']) }}</div>
                </div>
                <div class="low-bar text-center" style="width: {{ $data['rsf_percent_data']['low_percent'] }}%;">                    
                    <div class="title font-bold">Low</div>
                    <div class="value">{{ number_format($data['rsf_percent_data']['low_hours']) }}</div>
                </div>
            </div>
        </div>
        <div class="third_part p-4" style="background: white;">
            <div class="tableContainer">
                <table id="proximityActivityTable" class="table table-sm">
                    <thead>
                        <tr>
                            <th colspan="{{ $data['depth'] }}"></th>
                            <th colspan="3" class="text-center">Hours</th>
                            <th colspan="3" class="text-center">RSF</th>
                        </tr>
                        <tr>
                            @for ($i = 0; $i < $data['depth']; $i++)
                                <th style="border-bottom: none;padding-top: 20px;">{{ $data['thAry'][$i] }}</th>
                            @endfor
                            <th style="border-bottom: none;">High</th>
                            <th style="border-bottom: none;">Med</th>
                            <th style="border-bottom: none;">Low</th>
                            <th style="border-bottom: none;">High</th>
                            <th style="border-bottom: none;">Med</th>
                            <th style="border-bottom: none;">Low</th>
                        </tr>
                        <tr style="height:20px;">
                            @for ($i = 0; $i < $data['depth']; $i++)
                                <th class="jump-th" style="border-top: none;">
                                    <div class="flex justify-center jump-btn">
                                        @if ($i == $data['depth'] - 1) 
                                            <svg onclick="JumpToQuestionsByDepth({{ $i + 2 }});" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M328 544h152v152c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V544h152c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H544V328c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v152H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8z" fill="currentColor"/><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-40 728H184V184h656v656z" fill="currentColor"/></svg>
                                        @else
                                            <svg onclick="JumpToQuestionsByDepth({{ $i + 1 }});" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M328 544h368c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8z" fill="currentColor"/><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-40 728H184V184h656v656z" fill="currentColor"/></svg>
                                        @endif
                                    </div>
                                </th>
                            @endfor
                            <th colspan="6" style="border-top:none;"></th>
                        </tr>
                    </thead>
                    <tbody class="text-sm">
                        @foreach ($data['rows'] as $row)
                            @php
                                $questionDescAry = explode('..', $row['question_desc']);
                            @endphp
                            <tr>
                                @for ($i = 0; $i < $data['depth']; $i++)
                                    <td class="questionDescTD{{ $i }}" data-option="{{ $questionDescAry[0] }}">{{ $questionDescAry[$i] }}</td>
                                @endfor
                                <td>
                                    @if ($row['high_hours'] > 0)
                                    <div class="flex items-center">
                                        <div class="high-bar" style="width: {{ 60 * $row['high_hours'] / $data['max_high_hours'] }}%;height:15px;margin-right:1px;"></div>
                                        <div class="text-high">{{ number_format($row['high_hours']) }}</div>
                                    </div>                                        
                                    @endif
                                </td>
                                <td>
                                    @if ($row['med_hours'] > 0)
                                    <div class="flex items-center">
                                        <div class="med-bar" style="width: {{ 60 * $row['med_hours'] / $data['max_med_hours'] }}%;height:15px;margin-right:1px;"></div>
                                        <div class="text-med">{{ number_format($row['med_hours']) }}</div>
                                    </div>                                        
                                    @endif
                                </td>
                                <td>
                                    @if ($row['low_hours'] > 0)
                                    <div class="flex items-center">
                                        <div class="low-bar" style="width: {{ 60 * $row['low_hours'] / $data['max_low_hours'] }}%;height:15px;margin-right:1px;"></div>
                                        <div class="text-low">{{ number_format($row['low_hours']) }}</div>
                                    </div>                                        
                                    @endif
                                </td>
                                <td>
                                    @if ($row['high_rsf'] > 0)
                                    <div class="flex items-center">
                                        <div class="high-bar" style="width: {{ 60 * $row['high_rsf'] / $data['max_high_rsf'] }}%;height:15px;margin-right:1px;"></div>
                                        <div class="text-high">${{ number_format($row['high_rsf']) }}</div>
                                    </div>                                        
                                    @endif
                                </td>
                                <td>
                                    @if ($row['med_rsf'] > 0)
                                    <div class="flex items-center">
                                        <div class="med-bar" style="width: {{ 60 * $row['med_rsf'] / $data['max_med_rsf'] }}%;height:15px;margin-right:1px;"></div>
                                        <div class="text-med">${{ number_format($row['med_rsf']) }}</div>
                                    </div>                                        
                                    @endif
                                </td>
                                <td>
                                    @if ($row['low_rsf'] > 0)
                                    <div class="flex items-center">
                                        <div class="low-bar" style="width: {{ 60 * $row['low_rsf'] / $data['max_low_rsf'] }}%;height:15px;margin-right:1px;"></div>
                                        <div class="text-low">${{ number_format($row['low_rsf']) }}</div>
                                    </div>                                        
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="copyright_div" class="flex justify-begin items-center" style="">
        <div>
            <img src="{{asset('imgs/logo-new-small_rev.png')}}" style="height:60px" alt="">
        </div>
        <div>
            <a href="http://www.revelationlegal.com">http://www.revelationlegal.com</a> <br>
            <span>&copy; ofPartner LLC, All Rights Reserved. Report Generated @php echo date('m/d/Y') @endphp</span>
        </div>
    </div>
    <div id="headerDiv"></div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generatePDFModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body flex items-center justify-center" style="height: 150px;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating PDF...
                </div>
                <div class="modal-footer">
                    <button class="btn btn-revelation-primary" onclick="generatePDF();" disabled>Download</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generateExcelModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body flex items-center justify-center" style="height: 150px;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating Excel file ...
                </div>
                <div class="modal-footer">
                    <a class="btn btn-revelation-primary disabled" href="javascript:void(0);">Download</a>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-mask"></div>
    <script>
        var survey_id = @php echo $data['survey']->survey_id; @endphp;

        var highColor = "#e15659";
        var medColor = "#f28d36";
        var lowColor = "#4e7aa5";

        var imgData_1, imgData_2, imgData_3, imgData_4, copyrightData, headerData;

        let numberFormatter = new Intl.NumberFormat('en-US');

        $(document).ready(function () {
            mask_height = $('body').height();
            $('.loading-mask').css('height', mask_height);
            $('.loading-mask').show();
            for (let i = 0; i < depthQuestion; i++) {
                var span = 1;
                var prevTD = "";
                var prevTDVal = "";
                var prevTDOption = "";
                $(`td.questionDescTD${i}`).each(function() {
                    var $this = $(this);
                    if ($this.text() == prevTDVal && $this.attr('data-option') == prevTDOption) { // check value of previous td text
                        span++;
                        if (prevTD != "") {
                            prevTD.attr("rowspan", span); // add attribute to previous td
                            $this.remove(); // remove current td
                        }
                    } else {
                        prevTD     = $this; // store current td
                        prevTDVal  = $this.text();
                        prevTDOption  = $this.attr('data-option');
                        span       = 1;
                    }
                });
            }
            $('.loading-mask').hide();
        });

        // Handle the pdf button click, generate image data from the body
        $('#pdfBtn').click(function () {
            $('#generatePDFModal').modal('show');
            source = $('#participantProximityContent .first_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_1 = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#participantProximityContent .second_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_2 = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            // Copyright
            source = $('#copyright_div');
            html2canvas(source, {
                onrendered: function (canvas) {
                    copyrightData = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#headerDiv');
            html2canvas(source, {
                onrendered: function (canvas) {
                    headerData = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#participantProximityContent .third_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_3 = canvas.toDataURL('image/jpeg', 1.0);
                }
            }).then(function () {
                $('#generatePDFModal .modal-body').html('Generated a PDF');
                $('#generatePDFModal .btn').attr('disabled', false);
            });
        });

        $('#generatePDFModal').on('hidden.bs.modal', function () {
            $('#generatePDFModal').modal('hide');
            $('#generatePDFModal .modal-body').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating PDF...`);
            $('#generatePDFModal .btn').attr('disabled', true);
        });

        $('#generateExcelModal').on('hidden.bs.modal', function () {
            $('#generateExcelModal').modal('hide');
            $('#generateExcelModal .modal-body').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating Excel file...`);
            $('#generateExcelModal .btn').attr('href', 'javascript:void(0);');
            $('#generateExcelModal .btn').addClass('disabled');
        });

        $('#generateExcelModal .btn').click(function () {
            $('#generateExcelModal').modal('hide');
        });

        /**
        * Generate pdf document of report
        *
        * @return {void}
        */
        function generatePDF () {
            let imgWidth = $('#participantProximityContent .first_part').outerWidth();
            pdfdoc = new jsPDF('p', 'mm', 'a4');
            imgHeight1 = Math.round($('#participantProximityContent .first_part').outerHeight() * 190 / imgWidth);
            y = 10;
            position = y;
            doc_page = 1;

            pdfdoc.addImage(imgData_1, 'JPEG', 10, y, 190, imgHeight1);
            y += imgHeight1;

            imgHeight2 = Math.round($('#participantProximityContent .second_part').outerHeight() * 190 / imgWidth);
            pdfdoc.addImage(imgData_2, 'JPEG', 10, y, 190, imgHeight2);
            y += imgHeight2;

            imgHeight3 = Math.round($('#participantProximityContent .third_part').outerHeight() * 190 / imgWidth);
            pdfdoc.addImage(imgData_3, 'JPEG', 10, y, 190, imgHeight3);
            y += imgHeight3;

            pageHeight = pdfdoc.internal.pageSize.height - 20;
            heightLeft = y - pageHeight;

            while (heightLeft >= -pageHeight) {
                position = heightLeft - imgHeight3;
                pdfdoc.addPage();
                doc_page++;
                pdfdoc.addImage(imgData_3, 'JPEG', 10, position, 190, imgHeight3);
                heightLeft -= pageHeight;
            }

            pdfdoc.deletePage(doc_page);

            for (i = 1; i < doc_page; i++) {
                pdfdoc.setPage(i);
                pdfdoc.addImage(headerData, 'JPEG', 10, 0, 190, 10);
                pdfdoc.addImage(copyrightData, 'JPEG', 10, 287, 190, 10);
            }

            pdfdoc.save(`Proximity by Activity(Beta) - {{$data['survey']->survey_name}}`);
            $('#pdfBtn').html('Download PDF');
            $('#pdfBtn').prop('disabled', false);
            $('#generatePDFModal').modal('hide');
            $('#generatePDFModal .btn').attr('disabled', true);
        }

        function table_numberFormatter (value) {
            return numberFormatter.format(value);
        }

        function table_costFormatter (value) {
            return '$' + numberFormatter.format(value);
        }

        function table_highFormatter (value, row) {
            if (value > 0) {
                let width = Math.round(50 * value / max_hours);
                let show = numberFormatter.format(Math.round(value));
                let html = `<div class="flex items-center">
                        <div class="high-bar" style="width: ${width}%;height:15px;margin-right:1px;"></div>
                        <div>${show}</div>
                    </div>`;
                return html;
            } else {
                return '';
            }
        }

        function table_medFormatter (value, row) {
            if (value > 0) {
                let width = Math.round(50 * value / max_hours);
                let show = numberFormatter.format(Math.round(value));
                let html = `<div class="flex items-center">
                        <div class="med-bar" style="width: ${width}%;height:15px;margin-right:1px;"></div>
                        <div>${show}</div>
                    </div>`;
                return html;
            } else {
                return '';
            }
        }

        function table_lowFormatter (value, row) {
            if (value > 0) {
                let width = Math.round(50 * value / max_hours);
                let show = numberFormatter.format(Math.round(value));
                let html = `<div class="flex items-center">
                        <div class="low-bar" style="width: ${width}%;height:15px;margin-right:1px;"></div>
                        <div>${show}</div>
                    </div>`;
                return html;
            } else {
                return '';
            }
        }

        /**
        * Zoom in or out the report with the depth of taxonomy
        *
        * @param {number} depth
        * @return {void}
        */
        function JumpToQuestionsByDepth (depth) {
            depthQuestion = depth;

            $.ajax({
                url: "{{ route('realestate.filter-proximity-by-activity') }}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "survey_id": survey_id,
                    "position": JSON.stringify(options['position']),
                    "department": JSON.stringify(options['department']),
                    "group": JSON.stringify(options['group']),
                    "location": JSON.stringify(options['location']),
                    "category": JSON.stringify(options['category']),
                    "depthQuestion": depthQuestion,
                },
                dataType: 'json',
                beforeSend: function () {
                    mask_height = $('body').height();
                    $('.loading-mask').css('height', mask_height);
                    $('.loading-mask').show();
                    $('.dropdown-menu').removeClass('show');
                },
                success: function (res) {
                    if (res.rows == 404) {
                        Toast.fire({
                            icon: 'error',
                            title: 'No more record.'
                        });
                    } else {
                        rows = res.rows;
                        $tableContainer = $('.tableContainer');

                        strHtml = ` <table id="proximityActivityTable" class="table table-sm">
                                        <thead>
                                            <tr>
                                                <th colspan="${depthQuestion}"></th>
                                                <th colspan="3" class="text-center">Hours</th>
                                                <th colspan="3" class="text-center">RSF</th>
                                            </tr>
                                            <tr>`;

                        for (let i = 0; i < depthQuestion; i++) {
                            strHtml += `<th style="border-bottom: none;padding-top: 20px;">${res.thAry[i]}</th>`;
                        }

                        strHtml += `<th style="border-bottom: none;">High</th>
                                    <th style="border-bottom: none;">Med</th>
                                    <th style="border-bottom: none;">Low</th>
                                    <th style="border-bottom: none;">High</th>
                                    <th style="border-bottom: none;">Med</th>
                                    <th style="border-bottom: none;">Low</th>
                                </tr>
                                <tr style="height:20px;">`;

                        for (let i = 0; i < depthQuestion; i++) {
                            strHtml += `<th class="jump-th" style="border-top: none;">
                                            <div class="flex justify-center jump-btn">`;
                            if (i == depthQuestion - 1) {
                                strHtml += `<svg onclick="JumpToQuestionsByDepth(${i + 2});" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M328 544h152v152c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V544h152c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H544V328c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v152H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8z" fill="currentColor"/><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-40 728H184V184h656v656z" fill="currentColor"/></svg>`;
                            } else {
                                strHtml += `<svg onclick="JumpToQuestionsByDepth(${i + 1});" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M328 544h368c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8z" fill="currentColor"/><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-40 728H184V184h656v656z" fill="currentColor"/></svg>`;
                            }
                            strHtml += `</div>
                                    </th>`;
                        }

                        strHtml += `<th colspan="6" style="border-top:none;"></th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm">`;                        
                        
                        for (let i in rows) {
                            strHtml += `<tr>`;
                            questionDescAry = rows[i].question_desc.split("..");
                            for (j = 0; j < depthQuestion ; j++) {
                                strHtml += `<td class="questionDescTD${j}" data-option="${questionDescAry[0]}" title="${questionDescAry[j]}">${questionDescAry[j]}</td>`;
                            } 
                            strHtml += `<td>`;
                            if (rows[i].high_hours > 0) {
                                strHtml += `<div class="flex items-center">
                                                <div class="high-bar" style="width: ${60 * rows[i].high_hours / res.max_high_hours}%;height:15px;margin-right:1px;"></div>
                                                <div class="text-high">${numberFormatter.format(Math.round(rows[i].high_hours))}</div>
                                            </div>`;
                            }
                            strHtml += `</td>`;
                            strHtml += `<td>`;
                            if (rows[i].med_hours > 0) {
                                strHtml += `<div class="flex items-center">
                                                <div class="med-bar" style="width: ${60 * rows[i].med_hours / res.max_med_hours}%;height:15px;margin-right:1px;"></div>
                                                <div class="text-med">${numberFormatter.format(Math.round(rows[i].med_hours))}</div>
                                            </div>`;
                            }
                            strHtml += `</td>`;
                            strHtml += `<td>`;
                            if (rows[i].low_hours > 0) {
                                strHtml += `<div class="flex items-center">
                                                <div class="low-bar" style="width: ${60 * rows[i].low_hours / res.max_low_hours}%;height:15px;margin-right:1px;"></div>
                                                <div class="text-low">${numberFormatter.format(Math.round(rows[i].low_hours))}</div>
                                            </div>`;
                            }
                            strHtml += `</td>`;
                            strHtml += `<td>`;
                            if (rows[i].high_rsf > 0) {
                                strHtml += `<div class="flex items-center">
                                                <div class="high-bar" style="width: ${60 * rows[i].high_rsf / res.max_high_rsf}%;height:15px;margin-right:1px;"></div>
                                                <div class="text-high">$${numberFormatter.format(Math.round(rows[i].high_rsf))}</div>
                                            </div>`;
                            }
                            strHtml += `</td>`;
                            strHtml += `<td>`;
                            if (rows[i].med_rsf > 0) {
                                strHtml += `<div class="flex items-center">
                                                <div class="med-bar" style="width: ${60 * rows[i].med_rsf / res.max_med_rsf}%;height:15px;margin-right:1px;"></div>
                                                <div class="text-med">$${numberFormatter.format(Math.round(rows[i].med_rsf))}</div>
                                            </div>`;
                            }
                            strHtml += `</td>`;
                            strHtml += `<td>`;
                            if (rows[i].low_rsf > 0) {
                                strHtml += `<div class="flex items-center">
                                                <div class="low-bar" style="width: ${60 * rows[i].low_rsf / res.max_low_rsf}%;height:15px;margin-right:1px;"></div>
                                                <div class="text-low">$${numberFormatter.format(Math.round(rows[i].low_rsf))}</div>
                                            </div>`;
                            }
                            strHtml += `</td>`;
                        }

                        strHtml += `</tbody>
                                </table>`;

                        $tableContainer.html(strHtml);

                        for (let i = 0; i < depthQuestion; i++) {
                            var span = 1;
                            var prevTD = "";
                            var prevTDVal = "";
                            var prevTDOption = "";
                            $(`td.questionDescTD${i}`).each(function() { 
                                var $this = $(this);
                                if ($this.text() == prevTDVal && $this.attr('data-option') == prevTDOption) { // check value of previous td text
                                    span++;
                                    if (prevTD != "") {
                                        prevTD.attr("rowspan", span); // add attribute to previous td
                                        $this.remove(); // remove current td
                                    }
                                } else {
                                    prevTD     = $this; // store current td 
                                    prevTDVal  = $this.text();
                                    prevTDOption  = $this.attr('data-option');
                                    span       = 1;
                                }
                            });
                        }

                    }

                    $('.loading-mask').hide();
                },
                error: function(request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        }
    </script>

@endsection
