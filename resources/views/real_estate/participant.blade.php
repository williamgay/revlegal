@extends('layouts.reports')
@section('content')
    <div class="flex justify-between items-center border-b">
        <h3 class="text-survey font-bold py-4 px-8 m-0 text-lg">Participant Proximity Report / {{ $survey->survey_name }}</h3>
        @if (\Auth::check() && \Auth::user()->hasPermission('surveyPrint', $survey))
            <button class="btn btn-revelation-primary mx-4" id="pdfBtn">Download PDF</button>            
        @endif
    </div>
    <div id="participantProximityContent">
        <div class="first_part" style="background: white;">
            @include('real_estate.partials.participantfilter')
        </div>
        <div class="second_part px-4 pt-4" style="background: white;">
            <h5>Hours by Proximity Factor</h5>
            <div id="proximity_bar" class="flex w-full">
                <div class="high-bar text-center" style="width: {{ $data['rsf_percent_data']['high_percent'] }}%;">
                    <div class="title font-bold">High</div>
                    <div class="value">{{ number_format($data['rsf_percent_data']['high_hours']) }}</div>
                </div>
                <div class="med-bar text-center" style="width: {{ $data['rsf_percent_data']['med_percent'] }}%;">                    
                    <div class="title font-bold">Med</div>
                    <div class="value">{{ number_format($data['rsf_percent_data']['med_hours']) }}</div>
                </div>
                <div class="low-bar text-center" style="width: {{ $data['rsf_percent_data']['low_percent'] }}%;">                    
                    <div class="title font-bold">Low</div>
                    <div class="value">{{ number_format($data['rsf_percent_data']['low_hours']) }}</div>
                </div>
            </div>
        </div>
        <div class="third_part px-4" style="background: white;">
            <div class="toolbar">
                @if (\Auth::check() && \Auth::user()->hasPermission('surveyExport', $survey))
                {{-- <button class="btn btn-revelation-primary" id="excelBtn">Export to Excel</button> --}}
                @endif
            </div>
            <div class="table-responsive">
                <table 
                    id="respTable" 
                    data-toggle="table"
                    data-search="true"
                    data-search-align="left"
                    data-custom-search="searchRespName"
                    data-button-class="btn-revelation-primary"
                    data-toolbar=".toolbar"
                    data-toolbar-align="right"
                    class="table table-striped table-sm text-xs w-full" 
                    cellspacing="0" 
                    width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm" data-sortable="true" data-field="employee_id" data-width="50" data-halign="center">Employeed ID</th>
                            <th class="th-sm" data-sortable="true" data-field="name" data-halign="center">Full Name</th>
                            <th class="th-sm" data-sortable="true" data-field="category" data-halign="center">Category (group)</th>
                            <th class="th-sm" data-sortable="true" data-field="department" data-halign="center">Department</th>
                            <th class="th-sm" data-sortable="true" data-field="location" data-halign="center">Location</th>
                            <th class="th-sm" data-sortable="true" data-field="position" data-halign="center">Position</th>
                            <th class="th-sm text-right" data-sortable="true" data-field="total_hours" data-formatter="table_numberFormatter" data-halign="center">Total Hours</th>
                            <th class="th-sm text-right" data-sortable="true" data-field="rsf_cost" data-formatter="table_costFormatter" data-halign="center">Total RSF Cost</th>
                            <th class="th-sm pl-0" data-sortable="true" data-field="high_hours" data-formatter="table_highFormatter" data-width="150" data-halign="center">High</th>
                            <th class="th-sm pl-0" data-sortable="true" data-field="med_hours" data-formatter="table_medFormatter" data-width="150" data-halign="center">Med</th>
                            <th class="th-sm pl-0" data-sortable="true" data-field="low_hours" data-formatter="table_lowFormatter" data-width="150" data-halign="center">Low</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['resps'] as $resp)
                            <tr>
                                <td>{{ $resp->cust_1 }}</td>
                                <td>{{ $resp->resp_last }}, {{ $resp->resp_first }}</td>
                                <td>{{ $resp->cust_5 }}</td>
                                <td>{{ $resp->cust_4 }}</td>
                                <td>{{ $resp->cust_6 }}</td>
                                <td>{{ $resp->cust_3 }}</td>
                                <td>{{ $resp->total_hours }}</td>
                                <td>{{ $resp->rsf_cost }}</td>
                                <td>{{ $resp->prox_high_hours }}</td>
                                <td>{{ $resp->prox_medium_hours }}</td>
                                <td>{{ $resp->prox_low_hours }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="copyright_div" class="flex justify-begin items-center" style="">
        <div>
            <img src="{{asset('imgs/logo-new-small_rev.png')}}" style="height:60px" alt="">
        </div>
        <div>
            <a href="http://www.revelationlegal.com">http://www.revelationlegal.com</a> <br>
            <span>&copy; ofPartner LLC, All Rights Reserved. Report Generated @php echo date('m/d/Y') @endphp</span>
        </div>
    </div>
    <div id="headerDiv"></div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generatePDFModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body flex items-center justify-center" style="height: 150px;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating PDF...
                </div>
                <div class="modal-footer">
                    <button class="btn btn-revelation-primary" onclick="generatePDF();" disabled>Download</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generateExcelModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body flex items-center justify-center" style="height: 150px;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating Excel file ...
                </div>
                <div class="modal-footer">
                    <a class="btn btn-revelation-primary disabled" href="javascript:void(0);">Download</a>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-mask"></div>
    <script>
        var survey_id = @php echo $data['survey']->survey_id; @endphp;

        var highColor = "#e15659";
        var medColor = "#f28d36";
        var lowColor = "#4e7aa5";

        var imgData_1, imgData_2, imgData_3, imgData_4, copyrightData, headerData;

        let numberFormatter = new Intl.NumberFormat('en-US');

        $(document).ready(function () {
            $('#respTable').bootstrapTable({
                formatSearch: function () {
                    return 'Search Name'
                }
            });
        });

        // Handle the event of excel button click
        $('#excelBtn').click(function () {
            let tableData = $('#respTable').bootstrapTable('getData');
            $.ajax({
                url: '{{ route('realestate.exportParticipantExcel') }}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "survey_id": survey_id,
                    "tableData": JSON.stringify(tableData)
                },
                dataType: 'json',
                beforeSend: function () {
                    $('#generateExcelModal').modal('show');
                },
                success: function (res) {
                    $('#generateExcelModal .modal-body').html('Generated an Excel file');
                    $('#generateExcelModal .btn').attr('href', res.url);
                    $('#generateExcelModal .btn').attr('download', res.filename);
                    $('#generateExcelModal .btn').removeClass('disabled');
                },
                error: function(request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        });

        // Handle the pdf button click, generate image data from the body
        $('#pdfBtn').click(function () {
            $('#generatePDFModal').modal('show');
            source = $('#participantProximityContent .first_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_1 = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#participantProximityContent .second_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_2 = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            // Copyright
            source = $('#copyright_div');
            html2canvas(source, {
                onrendered: function (canvas) {
                    copyrightData = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#headerDiv');
            html2canvas(source, {
                onrendered: function (canvas) {
                    headerData = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#participantProximityContent .third_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_3 = canvas.toDataURL('image/jpeg', 1.0);
                }
            }).then(function () {
                $('#generatePDFModal .modal-body').html('Generated a PDF');
                $('#generatePDFModal .btn').attr('disabled', false);
            });
        });

        $('#generatePDFModal').on('hidden.bs.modal', function () {
            $('#generatePDFModal').modal('hide');
            $('#generatePDFModal .modal-body').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating PDF...`);
            $('#generatePDFModal .btn').attr('disabled', true);
        });

        $('#generateExcelModal').on('hidden.bs.modal', function () {
            $('#generateExcelModal').modal('hide');
            $('#generateExcelModal .modal-body').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating Excel file...`);
            $('#generateExcelModal .btn').attr('href', 'javascript:void(0);');
            $('#generateExcelModal .btn').addClass('disabled');
        });

        $('#generateExcelModal .btn').click(function () {
            $('#generateExcelModal').modal('hide');
        });

        /**
        * Generate pdf document of report
        *
        * @return {void}
        */
        function generatePDF () {
            let imgWidth = $('#participantProximityContent .first_part').outerWidth();
            pdfdoc = new jsPDF('p', 'mm', 'a4');
            imgHeight1 = Math.round($('#participantProximityContent .first_part').outerHeight() * 190 / imgWidth);
            y = 10;
            position = y;
            doc_page = 1;

            pdfdoc.addImage(imgData_1, 'JPEG', 10, y, 190, imgHeight1);
            y += imgHeight1;

            imgHeight2 = Math.round($('#participantProximityContent .second_part').outerHeight() * 190 / imgWidth);
            pdfdoc.addImage(imgData_2, 'JPEG', 10, y, 190, imgHeight2);
            y += imgHeight2;

            imgHeight3 = Math.round($('#participantProximityContent .third_part').outerHeight() * 190 / imgWidth);
            pdfdoc.addImage(imgData_3, 'JPEG', 10, y, 190, imgHeight3);
            y += imgHeight3;

            pageHeight = pdfdoc.internal.pageSize.height - 20;
            heightLeft = y - pageHeight;

            while (heightLeft >= -pageHeight) {
                position = heightLeft - imgHeight3;
                pdfdoc.addPage();
                doc_page++;
                pdfdoc.addImage(imgData_3, 'JPEG', 10, position, 190, imgHeight3);
                heightLeft -= pageHeight;
            }

            pdfdoc.deletePage(doc_page);

            for (i = 1; i < doc_page; i++) {
                pdfdoc.setPage(i);
                pdfdoc.addImage(headerData, 'JPEG', 10, 0, 190, 10);
                pdfdoc.addImage(copyrightData, 'JPEG', 10, 287, 190, 10);
            }

            pdfdoc.save(`Participant Proximity Report({{$data['survey']->survey_name}})`);
            $('#pdfBtn').html('Download PDF');
            $('#pdfBtn').prop('disabled', false);
            $('#generatePDFModal').modal('hide');
            $('#generatePDFModal .btn').attr('disabled', true);
        }

        function searchRespName (data, text) {
            return data.filter(function (row) {
                return row.name.indexOf(text) > -1;
            });
        }

        function table_numberFormatter (value) {
            return numberFormatter.format(value);
        }

        function table_costFormatter (value) {
            return '$' + numberFormatter.format(value);
        }

        function table_highFormatter (value, row) {
            if (value > 0) {
                let width = Math.round(50 * value / max_hours);
                let show = numberFormatter.format(Math.round(value));
                let html = `<div class="flex items-center">
                        <div class="high-bar" style="width: ${width}%;height:15px;margin-right:1px;"></div>
                        <div>${show}</div>
                    </div>`;
                return html;
            } else {
                return '';
            }
        }

        function table_medFormatter (value, row) {
            if (value > 0) {
                let width = Math.round(50 * value / max_hours);
                let show = numberFormatter.format(Math.round(value));
                let html = `<div class="flex items-center">
                        <div class="med-bar" style="width: ${width}%;height:15px;margin-right:1px;"></div>
                        <div>${show}</div>
                    </div>`;
                return html;
            } else {
                return '';
            }
        }

        function table_lowFormatter (value, row) {
            if (value > 0) {
                let width = Math.round(50 * value / max_hours);
                let show = numberFormatter.format(Math.round(value));
                let html = `<div class="flex items-center">
                        <div class="low-bar" style="width: ${width}%;height:15px;margin-right:1px;"></div>
                        <div>${show}</div>
                    </div>`;
                return html;
            } else {
                return '';
            }
        }
    </script>

@endsection
