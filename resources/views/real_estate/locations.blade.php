@extends('layouts.reports')
@section('content')
    <div class="flex justify-between items-center border-b">
        <h3 class="text-survey font-bold pt-4 px-4 text-lg">{{ __('Locations') }}</h3>
    </div>
    <div class="px-4 py-2">
        <button id="addLocationBtn" class="btn btn-revelation-primary flex items-center justify-end mb-2"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;display: inline;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="currentColor"><path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/></g></svg> New Location</button>
        <table class="table" id="location_table">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Location Name</th>
                    <th>Current</th>
                    <th>Adjacent</th>
                    <th>Regional</th>
                    <th>Other</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data['locations'] as $location)
                    <tr id="row-{{ $location->location_id }}">
                        <td>
                            <button data-id="{{ $location->location_id }}" class="btn btn-sm edit-btn btn-revelation-primary"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M18.988 2.012l3 3L19.701 7.3l-3-3zM8 16h3l7.287-7.287l-3-3L8 13z" fill="currentColor"/><path d="M19 19H8.158c-.026 0-.053.01-.079.01c-.033 0-.066-.009-.1-.01H5V5h6.847l2-2H5c-1.103 0-2 .896-2 2v14c0 1.104.897 2 2 2h14a2 2 0 0 0 2-2v-8.668l-2 2V19z" fill="currentColor"/></svg></button>
                            <button data-id="{{ $location->location_id }}" class="btn btn-sm remove-btn btn-revelation-danger"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M12 4h3c.6 0 1 .4 1 1v1H3V5c0-.6.5-1 1-1h3c.2-1.1 1.3-2 2.5-2s2.3.9 2.5 2zM8 4h3c-.2-.6-.9-1-1.5-1S8.2 3.4 8 4zM4 7h11l-.9 10.1c0 .5-.5.9-1 .9H5.9c-.5 0-.9-.4-1-.9L4 7z" fill="currentColor"/></svg></button>
                        </td>
                        <td>{{ $location->location }}</td>
                        <td>{{ $location->location_Current }}</td>
                        <td>{{ $location->location_Adjacent }}</td>
                        <td>{{ $location->location_Regional }}</td>
                        <td>{{ $location->location_OTHER }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div id="editLocationModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Add New Location</strong>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="location_id">
                    <div class="form-group">
                        <label for="addpage_title">Location Name</label>
                        <input type="text" class="form-control" id="location" placeholder="Location Name" required>
                    </div>
                    <div class="form-group">
                        <label for="addpage_title">Current Rate</label>
                        <input type="number" class="form-control" id="location_Current" step="0.01" placeholder="Current Rate" required>
                    </div>
                    <div class="form-group">
                        <label for="addpage_title">Adjacent Rate</label>
                        <input type="number" class="form-control" id="location_Adjacent" step="0.01" placeholder="Adjacent Rate" required>
                    </div>
                    <div class="form-group">
                        <label for="addpage_title">Regional Rate</label>
                        <input type="number" class="form-control" id="location_Regional" step="0.01" placeholder="Regional Rate" required>
                    </div>
                    <div class="form-group">
                        <label for="addpage_title">Other Rate</label>
                        <input type="number" class="form-control" id="location_OTHER" step="0.01" placeholder="Other Rate" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-revelation-primary" id="saveLocation">Save Changes</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var survey_id = {{ $data['survey']->survey_id }};
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            },
        });

        function init () {
            $('#addLocationBtn').click(function() {
                $('#editLocationModal input').val('');
                $('#editLocationModal').modal('show');
                $('#editLocationModal .modal-header strong').html('Add New Location');
            });
            
            $('.edit-btn').click(function() {
                $('#editLocationModal input').val('');
                let location_id = $(this).attr('data-id');
                $('#location_id').val(location_id);
                $('#location').val($(`#row-${location_id} td:eq(1)`).text());
                $('#location_Current').val($(`#row-${location_id} td:eq(2)`).text());
                $('#location_Adjacent').val($(`#row-${location_id} td:eq(3)`).text());
                $('#location_Regional').val($(`#row-${location_id} td:eq(4)`).text());
                $('#location_OTHER').val($(`#row-${location_id} td:eq(5)`).text());
                $('#editLocationModal').modal('show');
                $('#editLocationModal .modal-header strong').html('Edit Location');
            });

            $('.remove-btn').click(function () {
                let location_id = $(this).attr('data-id');
                removeLocation(location_id);
            });

            $('#saveLocation').click(function () {                
                let location_id = $('#location_id').val();
                if (location_id != '') {
                    updateLocation();
                } else {
                    createLocation();
                }
            });
        }

        $(document).ready(function() {
            init();
        });
        
        function createLocation () {
            $.ajax({
                url: '{{ route('real-estate.create-location') }}',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'survey_id': survey_id,
                    'location': $('#location').val(),
                    'location_Current': $('#location_Current').val(),
                    'location_Adjacent': $('#location_Adjacent').val(),
                    'location_Regional': $('#location_Regional').val(),
                    'location_OTHER': $('#location_OTHER').val(),
                },
                dataType: 'json',
                success: function (res) {
                    if (res.status == 200) {
                        $('#location_table tbody').append(`
                            <tr id="row-${res.new.location_id}">
                                <td>
                                    <button data-id="${res.new.location_id}" class="btn btn-sm edit-btn btn-revelation-primary"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M18.988 2.012l3 3L19.701 7.3l-3-3zM8 16h3l7.287-7.287l-3-3L8 13z" fill="currentColor"/><path d="M19 19H8.158c-.026 0-.053.01-.079.01c-.033 0-.066-.009-.1-.01H5V5h6.847l2-2H5c-1.103 0-2 .896-2 2v14c0 1.104.897 2 2 2h14a2 2 0 0 0 2-2v-8.668l-2 2V19z" fill="currentColor"/></svg></button>
                                    <button data-id="${res.new.location_id}" class="btn btn-sm remove-btn btn-revelation-danger"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M12 4h3c.6 0 1 .4 1 1v1H3V5c0-.6.5-1 1-1h3c.2-1.1 1.3-2 2.5-2s2.3.9 2.5 2zM8 4h3c-.2-.6-.9-1-1.5-1S8.2 3.4 8 4zM4 7h11l-.9 10.1c0 .5-.5.9-1 .9H5.9c-.5 0-.9-.4-1-.9L4 7z" fill="currentColor"/></svg></button>
                                </td>
                                <td>${res.new.location}</td>
                                <td>${res.new.location_Current}</td>
                                <td>${res.new.location_Adjacent}</td>
                                <td>${res.new.location_Regional}</td>
                                <td>${res.new.location_OTHER}</td>
                            </tr>`);
                        $('#editLocationModal').modal('hide');
                        Toast.fire({
                            icon: 'success',
                            title: 'A new location created successfully.'
                        });
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured while creating.'
                        });
                    }
                    init();
                },
                error: function (response) {
                    let errors = response.responseJSON.errors;
                    
                    errorHtml = '<ul style="list-style:unset;">';
                    for (const i in errors) {
                        errorHtml += `<li>${errors[i]}</li>`;
                    }
                    errorHtml += '</ul>';

                    if (response.responseJSON.message.indexOf('1062') > -1) {
                        title = 'Duplication Error';
                        errorHtml = 'The same location already exists';
                    } else {
                        title = response.responseJSON.message
                    }
                    Toast.fire({
                        icon: 'error',
                        title: title,
                        html: errorHtml,
                        customClass: {
                            icon: 'toast-icon'
                        }
                    })
                }
            });
        }
        
        function updateLocation () {
            $.ajax({
                url: '{{ route('real-estate.update-location') }}',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'location_id': $('#location_id').val(),
                    'location': $('#location').val(),
                    'location_Current': $('#location_Current').val(),
                    'location_Adjacent': $('#location_Adjacent').val(),
                    'location_Regional': $('#location_Regional').val(),
                    'location_OTHER': $('#location_OTHER').val(),
                },
                dataType: 'json',
                success: function (res) {
                    if (res.status == 200) {
                        $(`#row-${res.updated.location_id}`).html(`
                            <td>
                                <button data-id="${res.updated.location_id}" class="btn btn-sm edit-btn btn-revelation-primary"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M18.988 2.012l3 3L19.701 7.3l-3-3zM8 16h3l7.287-7.287l-3-3L8 13z" fill="currentColor"/><path d="M19 19H8.158c-.026 0-.053.01-.079.01c-.033 0-.066-.009-.1-.01H5V5h6.847l2-2H5c-1.103 0-2 .896-2 2v14c0 1.104.897 2 2 2h14a2 2 0 0 0 2-2v-8.668l-2 2V19z" fill="currentColor"/></svg></button>
                                <button data-id="${res.updated.location_id}" class="btn btn-sm remove-btn btn-revelation-danger"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><path d="M12 4h3c.6 0 1 .4 1 1v1H3V5c0-.6.5-1 1-1h3c.2-1.1 1.3-2 2.5-2s2.3.9 2.5 2zM8 4h3c-.2-.6-.9-1-1.5-1S8.2 3.4 8 4zM4 7h11l-.9 10.1c0 .5-.5.9-1 .9H5.9c-.5 0-.9-.4-1-.9L4 7z" fill="currentColor"/></svg></button>
                            </td>
                            <td>${res.updated.location}</td>
                            <td>${res.updated.location_Current}</td>
                            <td>${res.updated.location_Adjacent}</td>
                            <td>${res.updated.location_Regional}</td>
                            <td>${res.updated.location_OTHER}</td>`);
                        $('#editLocationModal').modal('hide');
                        Toast.fire({
                            icon: 'success',
                            title: 'A location has been updated successfully.'
                        });
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured while updating.'
                        });
                    }
                    init();
                },
                error: function (response) {
                    let errors = response.responseJSON.errors;
                    errorHtml = '<ul style="list-style:unset;">';
                    for (const i in errors) {
                        errorHtml += `<li>${errors[i]}</li>`;
                    }
                    errorHtml += '</ul>';
                    Toast.fire({
                        icon: 'error',
                        title: response.responseJSON.message,
                        html: errorHtml,
                        customClass: {
                            icon: 'toast-icon'
                        }
                    })
                }
            });
        }
            
        function removeLocation (location_id) {
            Swal.fire({
                title: 'Remove Survey',
                text: 'Are you sure to remove this survey?',
                icon: 'warning',
                showDenyButton: true,
                confirmButtonText: 'OK',
                denyButtonText: 'Cancel'
            }).then(function (isConfirmed) {
                if (isConfirmed.isConfirmed) {
                    $.ajax({
                        url: '{{ route('real-estate.destroy-location') }}',
                        type: 'POST',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'location_id': location_id
                        },
                        dataType: 'json',
                        success: function (res) {
                            if (res.status == 200) {
                                $(`#row-${location_id}`).remove();
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Removed successfully'
                                })
                            } else if (res.status == 400) {
                                Swal.fire({
                                    title: 'Error',
                                    text: res.message,
                                    icon: 'error',
                                    confirmButtonText: 'OK'
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'An error has been occured while removing.'
                                })
                            }
                            init();
                        },
                        error: function (response) {
                            let errors = response.responseJSON.errors;
                            errorHtml = '<ul style="list-style:unset;">';
                            for (const i in errors) {
                                errorHtml += `<li>${errors[i]}</li>`;
                            }
                            errorHtml += '</ul>';
                            Toast.fire({
                                icon: 'error',
                                title: response.responseJSON.message,
                                html: errorHtml,
                                customClass: {
                                    icon: 'toast-icon'
                                }
                            })
                        }
                    });
                }
            });
        }
    </script>
@endsection