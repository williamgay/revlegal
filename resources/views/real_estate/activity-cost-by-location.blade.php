@extends('layouts.reports')
@section('content')
    <div class="flex justify-between items-center border-b">
        <h3 class="text-survey font-bold py-4 px-8 m-0 text-lg">Activity Cost by Location / {{ $survey->survey_name }}</h3>
        <div>
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyExport', $survey))                
                <button class="btn btn-revelation-primary ml-2 mr-4" id="pdfBtn">Download PDF</button>                
            @endif
        </div>
    </div>
    <div id="individualContent">
        <div class="first_part">
            @include('real_estate.partials.activity-cost-by-location-filter')
        </div>
        <link rel="stylesheet" href="{{ asset('css/report-additional-style.css') }}">
        <div class="row second_part flex items-center justify-center" style="border-top:1px solid #dfdfdf;">
            
        </div>
        <div class="third_part" style="padding-top: 0;border-top: 3px solid #bfbfbf;">
            <div class="tableContainer">
                <table id="costbyLocationTable" 
                    class="table" 
                    style="width:96%;margin:25px 2%;display:none;">
                    <thead>
                        <tr>
                            <th style="border-bottom: none;padding-top:20px;width:120px;">Location</th>
                            @for ($i = 0; $i < $data['depth']; $i++)
                                <th style="border-bottom: none;">{{ $data['thAry'][$i] }}</th>
                            @endfor
                            <th class="text-right" style="border-bottom: none;">Employee Cost</th>
                            <th class="text-right" style="border-bottom: none;">RSF</th>
                            <th class="text-right" style="border-bottom: none;">Hours</th>
                            <th class="text-right" style="border-bottom: none;">RSF Cost(Current)</th>
                        </tr>
                        <tr style="height:20px">                            
                            <th style="border-top: none;"></th>
                            @for ($i = 0; $i < $data['depth']; $i++)
                                <th class="jump-th" style="border-top: none;">
                                    <div class="flex justify-center jump-btn">
                                        @if ($i == $data['depth'] - 1) 
                                            <svg onclick="JumpToQuestionsByDepth({{ $i + 2 }});" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M328 544h152v152c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V544h152c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H544V328c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v152H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8z" fill="currentColor"/><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-40 728H184V184h656v656z" fill="currentColor"/></svg>
                                        @else
                                            <svg onclick="JumpToQuestionsByDepth({{ $i + 1 }});" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M328 544h368c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8z" fill="currentColor"/><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-40 728H184V184h656v656z" fill="currentColor"/></svg>
                                        @endif
                                    </div>
                                </th>
                            @endfor
                            <th class="text-right" style="border-top: none;"></th>
                            <th class="text-right" style="border-top: none;"></th>
                            <th class="text-right" style="border-top: none;"></th>
                            <th class="text-right" style="border-top: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['costData'] as $locationData)
                            @foreach ($locationData['rows'] as $row)
                                @php
                                    $questionDescAry = explode('..', $row['question_desc']);
                                @endphp
                                <tr>
                                    <td class="questionDescTD0"><b>{{ $row['location'] }}</b></td>
                                    @for ($j = 0; $j < $data['depth']; $j++)
                                        <td class="questionDescTD{{ $j + 1 }}" data-option="{{ $row['location'] }}">{{ array_key_exists($j, $questionDescAry) ? $questionDescAry[$j] : '' }}</td>
                                    @endfor
                                    <td class="text-right">{{ number_format(round($row['employee_cost'])) }}</td>
                                    <td class="text-right">{{ number_format((float) $row['rsf']) }}</td>
                                    <td class="text-right">{{ number_format((float) $row['hours']) }}</td>
                                    <td class="text-right">{{ number_format((float) $row['rsf_cost_current']) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td style="border: none;"></td>
                                <td colspan="{{ $data['depth'] }}">Total</td>
                                <td class="text-right"><b>{{ number_format($locationData['total_employee_cost']) }}</b></td>
                                <td class="text-right"><b>{{ number_format($locationData['total_rsf']) }}</B></td>
                                <td class="text-right"><b>{{ number_format($locationData['total_hours']) }}</B></td>
                                <td class="text-right"><b>{{ number_format($locationData['total_cost_current']) }}</B></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><b>Grand Total</b></td>
                            <td colspan="{{ $data['depth'] }}"></td>
                            <td class="text-right"><b>{{ number_format($data['total_employee_cost']) }}</b></td>
                            <td class="text-right"><b>{{ number_format($data['total_rsf']) }}</b></td>
                            <td class="text-right"><b>{{ number_format($data['total_hours']) }}</b></td>
                            <td class="text-right"><b>{{ number_format($data['total_rsf_cost_current']) }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="copyright_div" class="flex justify-begin items-center" style="width:100%;background-color:white;padding:10px;font-size:11px;">
        <div>
            <img src="{{asset('imgs/logo-new-small_rev.png')}}" style="height:60px" alt="">
        </div>
        <div>
            <a href="http://www.revelationlegal.com">http://www.revelationlegal.com</a> <br>
            <span>&copy; ofPartner LLC, All Rights Reserved. Report Generated @php echo date('m/d/Y') @endphp</span>
        </div>
    </div>
    <div id="headerDiv" style="background-color: white;height:40px;width:100%;"></div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generatePDFModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body flex items-center justify-center" style="height: 150px;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating PDF...
                </div>
                <div class="modal-footer">
                    <button class="btn btn-revelation-primary" onclick="generatePDF();" disabled>Download</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generateExcelModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body flex items-center justify-center" style="height: 150px;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating Excel file ...
                </div>
                <div class="modal-footer">
                    <a class="btn btn-revelation-primary disabled" href="javascript:void(0);">Download</a>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-mask"></div>
    <script>
        var survey_id = @php echo $data['survey'] -> survey_id; @endphp;

        var imgData_1, imgData_2, imgData_3, imgData_4, copyrightData, headerData;

        let formatter = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'USD',
                        minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
                        maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
                    });

        let numberFormatter = new Intl.NumberFormat('en-US');

        // Handle the pdf button click, generate image data from the body
        $('#pdfBtn').click(function () {
            $('#generatePDFModal').modal('show');
            source = $('#individualContent .first_part');
            html2canvas(source).then(function (canvas) {
                imgData_1 = canvas.toDataURL('image/jpeg', 1.0);
            });
            source = $('#individualContent .second_part');
            html2canvas(source).then(function (canvas) {
                imgData_2 = canvas.toDataURL('image/jpeg', 1.0);
            });
            // Copyright
            source = $('#copyright_div');
            html2canvas(source).then(function (canvas) {
                copyrightData = canvas.toDataURL('image/jpeg', 1.0);
            });
            source = $('#headerDiv');
            html2canvas(source).then(function (canvas) {
                headerData = canvas.toDataURL('image/jpeg', 1.0);
            });
            source = $('#individualContent .third_part');
            html2canvas(source).then(function (canvas) {
                imgData_3 = canvas.toDataURL('image/jpeg', 1.0);
                $('#generatePDFModal .modal-body').html('Generated a PDF');
                $('#generatePDFModal .btn').attr('disabled', false);
            });
        });

        $(document).ready(function () {
            mask_height = $('body').height();
            $('.loading-mask').css('height', mask_height);
            $('.loading-mask').show();
            for (let i = 0; i < depthQuestion; i++) {
                var span = 1;
                var prevTD = "";
                var prevTDVal = "";
                var prevTDOption = "";
                $(`td.questionDescTD${i}`).each(function() {
                    var $this = $(this);
                    if ($this.text() == prevTDVal && $this.attr('data-option') == prevTDOption) { // check value of previous td text
                        span++;
                        if (prevTD != "") {
                            prevTD.attr("rowspan", span); // add attribute to previous td
                            $this.remove(); // remove current td
                        }
                    } else {
                        prevTD     = $this; // store current td
                        prevTDVal  = $this.text();
                        prevTDOption  = $this.attr('data-option');
                        span       = 1;
                    }
                });
            }
            $('#costbyLocationTable').css('display', 'table');
            $('.loading-mask').hide();
        });

        /**
        * Generate pdf document of report
        *
        * @return {void}
        */
        function generatePDF () {
            let imgWidth = $('#individualContent .first_part').outerWidth();
            pdfdoc = new jsPDF('p', 'mm', 'a4');
            imgHeight1 = Math.round($('#individualContent .first_part').outerHeight() * 190 / imgWidth);
            y = 10;
            position = y;
            doc_page = 1;

            pdfdoc.addImage(imgData_1, 'JPEG', 10, y, 190, imgHeight1);
            y += imgHeight1;

            imgHeight2 = Math.round($('#individualContent .second_part').outerHeight() * 190 / imgWidth);
            pdfdoc.addImage(imgData_2, 'JPEG', 10, y, 190, imgHeight2);
            y += imgHeight2;

            imgHeight3 = Math.round($('#individualContent .third_part').outerHeight() * 190 / imgWidth);
            pdfdoc.addImage(imgData_3, 'JPEG', 10, y, 190, imgHeight3);
            y += imgHeight3;

            pageHeight = pdfdoc.internal.pageSize.height - 20;
            heightLeft = y - pageHeight;

            while (heightLeft >= -pageHeight) {
                position = heightLeft - imgHeight3;
                pdfdoc.addPage();
                doc_page++;
                pdfdoc.addImage(imgData_3, 'JPEG', 10, position, 190, imgHeight3);
                heightLeft -= pageHeight;
            }

            pdfdoc.deletePage(doc_page);

            for (i = 1; i < doc_page; i++) {
                pdfdoc.setPage(i);
                pdfdoc.addImage(headerData, 'JPEG', 10, 0, 190, 10);
                pdfdoc.addImage(copyrightData, 'JPEG', 10, 287, 190, 10);
            }

            pdfdoc.save(`Activity Cost by Location Report({{$data['survey']->survey_name}})`);
            $('#pdfBtn').html('Download PDF');
            $('#pdfBtn').prop('disabled', false);
            $('#generatePDFModal').modal('hide');
            $('#generatePDFModal .btn').attr('disabled', true);
        }
        /**
        * Zoom in or out the report with the depth of taxonomy
        *
        * @param {number} depth
        * @return {void}
        */
        function JumpToQuestionsByDepth (depth) {
            depthQuestion = depth;

            $.ajax({
                url: "{{ route('realestate.filter-activity-cost-by-location') }}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "survey_id": survey_id,
                    "position": JSON.stringify(options['position']),
                    "department": JSON.stringify(options['department']),
                    "group": JSON.stringify(options['group']),
                    "location": JSON.stringify(options['location']),
                    "proximity": options['proximity'],
                    "depthQuestion": depthQuestion,
                },
                dataType: 'json',
                beforeSend: function () {
                    mask_height = $('body').height();
                    $('.loading-mask').css('height', mask_height);
                    $('.loading-mask').show();
                    $('.dropdown-menu').removeClass('show');
                },
                success: function (res) {
                    if (res.costData == 404) {
                        Toast.fire({
                            icon: 'error',
                            title: 'No more record.'
                        });
                    } else {
                        costData = res.costData;
                        $tableContainer = $('.tableContainer');

                        strHtml = `<table id="costbyLocationTable" 
                                        class="table" 
                                        style="width:96%;margin:25px 2%;display:none;">
                                        <thead>
                                            <tr>
                                                <th style="border-bottom: none;padding-top:20px;width:120px;">Location</th>`;

                        for (let i = 0; i < depthQuestion; i++) {
                            strHtml += `<th style="border-bottom: none;">${res.thAry[i]}</th>`;
                        }

                        strHtml += `<th class="text-right" style="border-bottom: none;">Employee Cost</th>
                                    <th class="text-right" style="border-bottom: none;">RSF</th>
                                    <th class="text-right" style="border-bottom: none;">Hours</th>
                                    <th class="text-right" style="border-bottom: none;">RSF Cost(Current)</th>
                                </tr>
                                <tr style="height:20px">                            
                                    <th style="border-top: none;"></th>`;

                        for (let i = 0; i < depthQuestion; i++) {
                            strHtml += `<th class="jump-th" style="border-top: none;">
                                            <div class="flex justify-center jump-btn">`;
                            if (i == depthQuestion - 1) {
                                strHtml += `<svg onclick="JumpToQuestionsByDepth(${i + 2});" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M328 544h152v152c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V544h152c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H544V328c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v152H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8z" fill="currentColor"/><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-40 728H184V184h656v656z" fill="currentColor"/></svg>`;
                            } else {
                                strHtml += `<svg onclick="JumpToQuestionsByDepth(${i + 1});" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M328 544h368c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8z" fill="currentColor"/><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32zm-40 728H184V184h656v656z" fill="currentColor"/></svg>`;
                            }
                            strHtml += `</div>
                                    </th>`;
                        }

                        strHtml += `<th class="text-right" style="border-top: none;"></th>
                                    <th class="text-right" style="border-top: none;"></th>
                                    <th class="text-right" style="border-top: none;"></th>
                                    <th class="text-right" style="border-top: none;"></th>
                                </tr>
                            </thead>
                            <tbody>`;                        
                        
                        for (const location in costData) {
                            rows = costData[location].rows;
                            
                            for (let i in rows) {
                                strHtml += `<tr>`;
                                strHtml += `<td class="questionDescTD0"><b>${rows[i].location}</b></td>`;
                                questionDescAry = rows[i].question_desc.split("..");
                                for (j = 0; j < depthQuestion ; j++) {
                                    strHtml += `<td class="questionDescTD${j + 1}" data-option="${rows[i].location}" title="${questionDescAry[j]}">${questionDescAry[j]}</td>`;
                                }                                
                                strHtml += `<td class="text-right">${numberFormatter.format(Math.round(rows[i].employee_cost))}</td>
                                            <td class="text-right">${numberFormatter.format(Math.round(rows[i].rsf))}</td>
                                            <td class="text-right">${numberFormatter.format(Math.round(rows[i].hours))}</td>
                                            <td class="text-right">${numberFormatter.format(Math.round(rows[i].rsf_cost_current))}</td>
                                        </tr>`;
                            }

                            strHtml += `<tr>
                                            <td style="border: none;"></td>
                                            <td colspan="${depthQuestion}">Total</td>
                                            <td class="text-right"><b>${numberFormatter.format(Math.round(costData[location].total_employee_cost))}</b></td>
                                            <td class="text-right"><b>${numberFormatter.format(Math.round(costData[location].total_rsf))}</b></td>
                                            <td class="text-right"><b>${numberFormatter.format(Math.round(costData[location].total_hours))}</b></td>
                                            <td class="text-right"><b>${numberFormatter.format(Math.round(costData[location].total_cost_current))}</b></td>
                                        </tr>`;
                        }

                        strHtml += `<tr>
                                        <td><b>Grand Total</b></td>
                                        <td colspan="${depthQuestion}"></td>
                                        <td class="text-right"><b>${numberFormatter.format(Math.round(res.total_employee_cost))}</b></td>
                                        <td class="text-right"><b>${numberFormatter.format(Math.round(res.total_rsf))}</b></td>
                                        <td class="text-right"><b>${numberFormatter.format(Math.round(res.total_hours))}</b></td>
                                        <td class="text-right"><b>${numberFormatter.format(Math.round(res.total_rsf_cost_current))}</b></td>
                                    </tr>`;

                        strHtml += `</tbody>
                                </table>`;

                        $tableContainer.html(strHtml);

                        for (let i = 0; i < depthQuestion; i++) {
                            var span = 1;
                            var prevTD = "";
                            var prevTDVal = "";
                            var prevTDOption = "";
                            $(`td.questionDescTD${i}`).each(function() { 
                                var $this = $(this);
                                if ($this.text() == prevTDVal && $this.attr('data-option') == prevTDOption) { // check value of previous td text
                                    span++;
                                    if (prevTD != "") {
                                        prevTD.attr("rowspan", span); // add attribute to previous td
                                        $this.remove(); // remove current td
                                    }
                                } else {
                                    prevTD     = $this; // store current td 
                                    prevTDVal  = $this.text();
                                    prevTDOption  = $this.attr('data-option');
                                    span       = 1;
                                }
                            });
                        }

                        $('#costbyLocationTable').css('display', 'table');
                    }

                    $('.loading-mask').hide();
                },
                error: function(request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        }
    </script>

@endsection
