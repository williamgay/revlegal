@extends('layouts.reports')
@section('content')
    <div class="flex justify-between items-center border-b">
        <h3 class="text-survey font-bold py-4 px-8 m-0 text-lg">Opportunity Summary Report / {{ $survey->survey_name }}</h3>
        <div>
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyPrint', $survey))                
                <button class="btn btn-revelation-primary ml-2 mr-4" id="pdfBtn">Download PDF</button>                
            @endif
        </div>
    </div>
    <div id="individualContent">
        <div class="first_part">
            @include('real_estate.partials.opportunity-summary-filter')
        </div>
        <link rel="stylesheet" href="{{ asset('css/report-additional-style.css') }}">
        <div class="row second_part flex items-center justify-center" style="padding:20px;border-top:1px solid #dfdfdf;">
            <table id="locationRatesTable" class="table table-sm table-striped w-50 m-0">
                <thead>
                    <tr>
                        <th class="text-right"></th>
                        <th class="text-right">Current</th>
                        <th class="text-right">Adjacent</th>
                        <th class="text-right">Regional</th>
                        <th class="text-right">Other</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['locationRates'] as $location)
                        <tr>
                            <td class="text-right"><b>{{ $location->location }}</b></td>
                            <td class="text-right">${{ number_format($location->location_Current, 2) }}</td>
                            <td class="text-right">${{ number_format($location->location_Adjacent, 2) }}</td>
                            <td class="text-right">${{ number_format($location->location_Regional, 2) }}</td>
                            <td class="text-right">${{ number_format($location->location_OTHER, 2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="third_part p-4" style="padding-top: 0;border-top: 3px solid #bfbfbf;">
            <div class="tableDiv">
                <table id="opportunitySummaryTable"
                    data-toggle="table">
                    <thead>
                        <tr>
                            <th class="text-center" colspan="4"><b>Current Cost</b></th>
                            <th class="border-none"></th>
                            <th class="text-center" colspan="6"><b>Potential Savings</b></th>
                        </tr>
                        <tr>
                            <th colspan="4"></th>
                            <th class="border-none"></th>
                            <th class="text-center" colspan="2">Adjacent</th>
                            <th class="text-center" colspan="2">Regional</th>
                            <th class="text-center" colspan="2">Other</th>
                        </tr>
                        <tr>
                            <th>Proximity Factor</th>
                            <th class="text-right" data-field="rsf" data-sortable="false" data-formatter="table_numberFormatter">RSF</th>
                            <th class="text-right" data-field="blended_rate" data-sortable="false" data-formatter="table_rateFormatter">Blended Rate*</th>
                            <th class="text-right" data-field="rsf_cost_current" data-sortable="false" data-formatter="table_costFormatter">RSF Cost(Current)</th>
                            <th class="border-none"></th>
                            <th class="text-right" data-field="rsf_cost_adjacent" data-sortable="false" data-formatter="table_costFormatter">(M)</th>
                            <th class="text-right" data-field="percent_adjacent" data-sortable="false" data-formatter="table_percentFormatter">.</th>
                            <th class="text-right" data-field="rsf_cost_regional" data-sortable="false" data-formatter="table_costFormatter">(L)</th>
                            <th class="text-right" data-field="percent_regional" data-sortable="false" data-formatter="table_percentFormatter">.</th>
                            <th class="text-right" data-field="rsf_cost_other" data-sortable="false" data-formatter="table_costFormatter">(LA)</th>
                            <th class="text-right" data-field="percent_other" data-sortable="false" data-formatter="table_percentFormatter">.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['summaryData'] as $prox => $row)
                            @php
                                $blended_rate = $row['rsf'] > 0 ? $row['rsf_cost_current'] / $row['rsf'] : 0;
                                $adjacent_percent = $row['rsf_cost_adjacent'] > 0 ? 100 * ($row['rsf_cost_current'] - $row['rsf_cost_adjacent']) / $row['rsf_cost_current'] : 0;
                                $regional_percent = $row['rsf_cost_regional'] > 0 ? 100 * ($row['rsf_cost_current'] - $row['rsf_cost_regional']) / $row['rsf_cost_current'] : 0;
                                $other_percent = $row['rsf_cost_other'] > 0 ? 100 * ($row['rsf_cost_current'] - $row['rsf_cost_other']) / $row['rsf_cost_current'] : 0;
                            @endphp
                            <tr>
                                <td><b>{{ ucfirst($prox) }}</b></td>
                                <td class="text-right {{ $prox }}-tr">{{ $row['rsf'] }}</td>
                                <td class="text-right {{ $prox }}-tr">{{ round($blended_rate, 2) }}</td>
                                <td class="text-right {{ $prox }}-tr">{{ $row['rsf_cost_current'] }}</td>
                                <td style="border: none;"></td>
                                <td class="text-right {{ $prox }}-tr">{{ $row['rsf_cost_adjacent'] > 0 ? $row['rsf_cost_current'] - $row['rsf_cost_adjacent'] : 0 }}</td>
                                <td class="text-right {{ $prox }}-tr">{{ round($adjacent_percent, 2) }}</td>
                                <td class="text-right {{ $prox }}-tr">{{ $row['rsf_cost_regional'] > 0 ? $row['rsf_cost_current'] - $row['rsf_cost_regional'] : 0 }}</td>
                                <td class="text-right {{ $prox }}-tr">{{ round($regional_percent, 2) }}</td>
                                <td class="text-right {{ $prox }}-tr">{{ $row['rsf_cost_other'] > 0 ? $row['rsf_cost_current'] - $row['rsf_cost_other'] : 0 }}</td>
                                <td class="text-right {{ $prox }}-tr">{{ round($other_percent, 2) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="copyright_div" class="flex justify-begin items-center" style="width:100%;background-color:white;padding:10px;font-size:11px;">
        <div>
            <img src="{{asset('imgs/logo-new-small_rev.png')}}" style="height:60px" alt="">
        </div>
        <div>
            <a href="http://www.revelationlegal.com">http://www.revelationlegal.com</a> <br>
            <span>&copy; ofPartner LLC, All Rights Reserved. Report Generated @php echo date('m/d/Y') @endphp</span>
        </div>
    </div>
    <div id="headerDiv" style="background-color: white;height:40px;width:100%;"></div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generatePDFModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body flex items-center justify-center" style="height: 150px;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating PDF...
                </div>
                <div class="modal-footer">
                    <button class="btn btn-revelation-primary" onclick="generatePDF();" disabled>Download</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generateExcelModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body flex items-center justify-center" style="height: 150px;">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp;&nbsp; Generating Excel file ...
                </div>
                <div class="modal-footer">
                    <a class="btn btn-revelation-primary disabled" href="javascript:void(0);">Download</a>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-mask"></div>
    <script>
        var survey_id = @php echo $data['survey'] -> survey_id; @endphp;
        var respData  = @php echo $data['resps']; @endphp;

        var imgData_1, imgData_2, imgData_3, imgData_4, copyrightData, headerData;

        let formatter = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'USD',
                        minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
                        maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
                    });

        let numberFormatter = new Intl.NumberFormat('en-US');

        // Handle the pdf button click, generate image data from the body
        $('#pdfBtn').click(function () {
            $('#generatePDFModal').modal('show');
            source = $('#individualContent .first_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_1 = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#individualContent .second_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_2 = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            // Copyright
            source = $('#copyright_div');
            html2canvas(source, {
                onrendered: function (canvas) {
                    copyrightData = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#headerDiv');
            html2canvas(source, {
                onrendered: function (canvas) {
                    headerData = canvas.toDataURL('image/jpeg', 1.0);
                }
            });
            source = $('#individualContent .third_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    imgData_3 = canvas.toDataURL('image/jpeg', 1.0);
                }
            }).then(function () {
                $('#generatePDFModal .modal-body').html('Generated a PDF');
                $('#generatePDFModal .btn').attr('disabled', false);
            });
        });

        $('#rsf_cost_sort').click(function () {
            val = $(this).val();
            $('.text-Adjacent').hide();
            $('.text-Regional').hide();
            $('.text-OTHER').hide();
            $(`.text-${val}`).show();
        });

        /**
        * Generate pdf document of report
        *
        * @return {void}
        */
        function generatePDF () {
            let imgWidth = $('#individualContent .first_part').outerWidth();
            pdfdoc = new jsPDF('p', 'mm', 'a4');
            imgHeight1 = Math.round($('#individualContent .first_part').outerHeight() * 190 / imgWidth);
            y = 10;
            position = y;
            doc_page = 1;

            pdfdoc.addImage(imgData_1, 'JPEG', 10, y, 190, imgHeight1);
            y += imgHeight1;

            imgHeight2 = Math.round($('#individualContent .second_part').outerHeight() * 190 / imgWidth);
            pdfdoc.addImage(imgData_2, 'JPEG', 10, y, 190, imgHeight2);
            y += imgHeight2;

            imgHeight3 = Math.round($('#individualContent .third_part').outerHeight() * 190 / imgWidth);
            pdfdoc.addImage(imgData_3, 'JPEG', 10, y, 190, imgHeight3);
            y += imgHeight3;

            pageHeight = pdfdoc.internal.pageSize.height - 20;
            heightLeft = y - pageHeight;

            while (heightLeft >= -pageHeight) {
                position = heightLeft - imgHeight3;
                pdfdoc.addPage();
                doc_page++;
                pdfdoc.addImage(imgData_3, 'JPEG', 10, position, 190, imgHeight3);
                heightLeft -= pageHeight;
            }

            pdfdoc.deletePage(doc_page);

            for (i = 1; i < doc_page; i++) {
                pdfdoc.setPage(i);
                pdfdoc.addImage(headerData, 'JPEG', 10, 0, 190, 10);
                pdfdoc.addImage(copyrightData, 'JPEG', 10, 287, 190, 10);
            }

            pdfdoc.save(`Opportunity Summary Report({{$data['survey']->survey_name}})`);
            $('#pdfBtn').html('Download PDF');
            $('#pdfBtn').prop('disabled', false);
            $('#generatePDFModal').modal('hide');
            $('#generatePDFModal .btn').attr('disabled', true);
        }

        function table_numberFormatter (value) {
            return numberFormatter.format(Math.round(value));
        }

        function table_rateFormatter (value) {
            return '$' + value;
        }

        function table_costFormatter (value) {
            if (value == 0) {
                return '';
            }            
            return '$' + numberFormatter.format(Math.round(value));
        }

        function table_percentFormatter (value) {
            if (value == 0) {
                return '';
            }
            return value + '%';
        }
    </script>

@endsection
