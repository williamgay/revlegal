@extends('layouts.admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    @if(\Auth::check() && \Auth::user()->is_admin)
        <div class="mt-2" style="display: none;">
            <div class="row alert alert-dismissible" id="message_flash_row" style="display: none;">
                <div class="col-12">
                    <h3 id="message_flash_text"></h3>
                </div>
            </div>
        </div>
        <div class="project-tab mt-3">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab">
                    <a class="nav-item nav-link active" id="nav-active-tab" data-toggle="tab" href="#nav-active" role="tab" aria-controls="nav-active" aria-selected="true">All Currently Active Users</a>
                    <a class="nav-item nav-link" id="nav-inactive-tab" data-toggle="tab" href="#nav-inactive" role="tab" aria-controls="nav-inactive" aria-selected="false">Currently Inactive Users</a>
                </div>
            </nav>
            <div class="tab-content" id=" nav-tabContent">
                <div class="tab-pane fade show active" id="nav-active" role="tabpanel" aria-labelledby="nav-active-tab">
                    <div class="mx-3">
                        <div class="mb-2">
                            <button class="btn btn-revelation-primary flex items-center" id="btnAddUser">
                                <svg style="display: inline;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><rect x="0" y="0" width="20" height="20" fill="none" stroke="none" /><path d="M15.989 19.129C16 17 13.803 15.74 11.672 14.822c-2.123-.914-2.801-1.684-2.801-3.334c0-.989.648-.667.932-2.481c.12-.752.692-.012.802-1.729c0-.684-.313-.854-.313-.854s.159-1.013.221-1.793c.064-.817-.398-2.56-2.301-3.095c-.332-.341-.557-.882.467-1.424c-2.24-.104-2.761 1.068-3.954 1.93c-1.015.756-1.289 1.953-1.24 2.59c.065.78.223 1.793.223 1.793s-.314.17-.314.854c.11 1.718.684.977.803 1.729c.284 1.814.933 1.492.933 2.481c0 1.65-.212 2.21-2.336 3.124C.663 15.53 0 17 .011 19.129C.014 19.766 0 20 0 20h16s-.014-.234-.011-.871zM17 10V7h-2v3h-3v2h3v3h2v-3h3v-2h-3z" fill="currentColor"/></svg> Add User
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Last Login</th>
                                        <th>Project Access</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="active_user_table">
                                    @foreach($active_users as $user)
                                        <tr>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->first_name }}</td>
                                            <td>{{ $user->last_name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ Carbon\Carbon::parse($user->last_login)->format('F d, Y h:ma') }}</td>
                                            <td>{!! $user->allowedProjects !!}</td>
                                            <td class="my-auto">
                                                <button id="active_user_{{ $user->id }}" data-user_id="{{ $user->id }}" class="btn deactivate-btn text-white btn-revelation-primary">Deactivate</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $active_users->links() }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-inactive" role="tabpanel" aria-labelledby="nav-inactive-tab">
                    <div class="m-3">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Deactivated At</th>
                                        <th>Project Access</th>
                                        <th colspan="2">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="inactive_user_table">
                                    @foreach($inactive_users as $user)
                                        <tr>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->first_name }}</td>
                                            <td>{{ $user->last_name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ Carbon\Carbon::parse($user->deleted_at)->format('F d, Y h:ma') }}</td>
                                            <td>{!! $user->allowedProjects !!}</td>
                                            <td>
                                                <button id="inactive_user_{{ $user->id }}" data-user_id="{{ $user->id }}" class="btn activate-btn text-white btn-revelation-primary">Activate</button>
                                            </td class="my-auto">
                                            <td class="my-auto">
                                                <button id="delete_user_{{ $user->id }}" data-user_id="{{ $user->id }}" class="btn delete-btn text-white btn-danger">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- {{ $inactive_users->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" tabindex="-1" role="dialog" id="addNewUser" aria-labelledby="New User" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-notify modal-warning" role="document">
                <!--Content-->
                <div class="modal-content">
                    <!--Header-->
                    <div class="modal-header text-center rl-modal-header">
                        <h4 id="manage_user_title" class="modal-title white-text w-100 font-weight-bold py-2">Add User</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="white-text">&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" style="height: 50vh; overflow-y: auto;">
                        <input type="hidden" id="user_id" name="user_id" value="">
                        <div class="md-form mb-2">
                            <label data-error="wrong" data-success="right" for="first_name">First Name</label>
                            <input type="text" id="first_name" class="form-control validate" placeholder="First Name">
                        </div>
                        <div class="md-form mb-2">
                            <label data-error="wrong" data-success="right" for="last_name">Last Name</label>
                            <input type="text" id="last_name" class="form-control validate" placeholder="Last Name">
                        </div>
                        <div class="md-form mb-2">
                            <label data-error="wrong" data-success="right" for="email">Email</label>
                            <input type="email" id="email" class="form-control validate" placeholder="Email">
                        </div>
                        <div class="md-form mb-2">
                            <label data-error="wrong" data-success="right" for="username">Username</label>
                            <input type="text" id="username" class="form-control validate" autocomplete="off" placeholder="Username">
                        </div>
                        <div class="md-form mb-2">
                            <label data-error="wrong" data-success="right" for="password">Password</label>
                            <input type="password" id="password" class="form-control validate" autocomplete="off" placeholder="Password - Leave empty to keep current password">
                        </div>
                        <hr>

                        <h5>Projects (optional)</h5>
                        <select id="projects" name="projects[]" multiple="multiple" class="form-control mb-1" style="display: block;">
                            @foreach(App\Models\Survey::all()->sortBy('survey_name') as $project)
                                <option value="{{ $project->survey_id }}">{{ $project->survey_name }}</option>
                            @endforeach
                        </select>

                        {{-- <h5 class="mt-2">Allowed Departments</h5>
                        <select id="departments" name="departments[]" multiple="multiple" class="form-control" style="display: block;">
                            @foreach(App\Models\Department::all()->unique('name')->sortBy('name') as $department)
                                <option value="{{ $department->name }}">{{ $department->name }}</option>
                            @endforeach
                        </select>

                        <h5 class="mt-2">Allowed Locations</h5>
                        <select id="locations" name="locations[]" multiple="multiple" class="form-control" style="display: block;">
                            @foreach(App\Models\SupportLocation::all()->unique('support_location_desc')->sortBy('support_location_desc') as $location)
                                <option value="{{ $location->support_location_desc }}">{{ $location->support_location_desc }}</option>
                            @endforeach
                        </select>

                        <h5 class="mt-2">Project Permissions</h5>
                        <select id="permissions" name="permissions[]" multiple="multiple" class="form-control" style="display: block;">
                            @foreach(App\Models\Permission::all()->unique('name')->sortBy('name') as $permission)
                                <option value="{{ $permission->name }}">{{ $permission->name }}</option>
                            @endforeach
                        </select> --}}
                    </div>

                    <!--Footer-->
                    <div class="modal-footer justify-content-center">
                        <button class="btn rounded waves-effect text-white" id="saveUser" style="background: #008EC1;">Save <i class="fa fa-paper-plane ml-1"></i></button>
                    </div>
                </div>
                <!--/.Content-->
            </div>
        </div>
    @else
        <script>
            Swal.fire({
                title: 'Permissions Required',
                text: 'You do not have permissions to view this page.',
                icon: 'error',
                confirmButtonText: 'OK',
            });
        </script>
    @endif

    @if(\Auth::check() && \Auth::user()->is_admin)
        <script type="text/javascript">
            var projects_data, departments_data, locations_data, permissions_data;
            $(function(){
                projects_data    = $('#projects').html();
                departments_data = $('#departments').html();
                locations_data   = $('#locations').html();
                permissions_data = $('#permissions').html();

                $('#btnAddUser').on('click', function () {
                    $('#addNewUser').modal('show');
                    $('#addNewUser input').val('');
                    
                    // Initialize select2
                    $('#projects').html(projects_data);
                    $('#departments').html(departments_data);
                    $('#permissions').html(permissions_data);
                    $('#locations').html(locations_data);

                    $('#projects').select2({ width: '100%'});
                    $('#departments').select2({ width: '100%'});
                    $('#permissions').select2({ width: '100%'});
                    $('#locations').select2({ width: '100%'});
                });

                $('#saveUser').on('click', function(){
                    saveUser();
                });

                //** DEACTIVATE USER CODE **//
                $(document).on('click', '.deactivate-btn', function(){
                    const id = $(this).data('user_id');
                    showLoader();
                    $.post(`/all-users/deactivate`, { user_id: id })
                        .done(function(data) {
                            hideLoader();

                            if(data.error) {

                                Swal.fire({
                                    title: 'Permissions Required',
                                    text: 'You do not have permission to perform this action.',
                                    icon: 'error',
                                    confirmButtonText: 'OK',
                                });

                            } else {
                                $('#active_user_' + id).parents('tr').fadeOut(300, 'linear', function(){
                                    $(this).remove();

                                    const lastLogin = new Date(data.deleted_at);
                                    let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(lastLogin);
                                    let mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(lastLogin);
                                    let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(lastLogin);
                                    let time = new Intl.DateTimeFormat('en', { hour: 'numeric', minute: 'numeric' }).format(lastLogin);

                                    $('#inactive_user_table').append(`
                                        <tr>
                                            <td>${data.username}</td>
                                            <td>${data.first_name}</td>
                                            <td>${data.last_name}</td>
                                            <td>${data.email}</td>
                                            <td>${mo} ${da}, ${ye} ${time}</td>
                                            <td>${data.allowedProjects}</td>
                                            <td class="my-auto">
                                                <button id="inactive_user_${data.id}" data-user_id="${data.id}" class="btn activate-btn btn-revelation-primary text-white btn-revelation-primary">Activate</button>
                                            </td>
                                            <td class="my-auto">
                                                <button id="delete_user_${data.id}" data-user_id="${data.id}" class="btn delete-btn text-white btn-danger">Delete</button>
                                            </td>
                                        </tr>
                                    `);
                                })
                            }
                        })
                        .catch(function(data){
                            console.log(data);
                            hideLoader();
                        })
                });

                //** ACTIVATE USER CODE **//
                $(document).on('click', '.activate-btn', function(){
                    const id = $(this).data('user_id');
                    showLoader();
                    $.post(`/all-users/activate`, { user_id: id })
                        .done(function(data){
                            hideLoader();

                            if(data.error) {

                            Swal.fire({
                                title: 'Permissions Required',
                                text: 'You do not have permission to perform this action.',
                                icon: 'error',
                                confirmButtonText: 'OK',
                            });

                            } else {
                                $('#inactive_user_' + id).parents('tr').fadeOut(300, 'linear', function(){
                                    $(this).remove();


                                    const lastLogin = new Date(data.last_login);
                                    let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(lastLogin);
                                    let mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(lastLogin);
                                    let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(lastLogin);
                                    let time = new Intl.DateTimeFormat('en', { hour: 'numeric', minute: 'numeric' }).format(lastLogin);

                                    $('#active_user_table').append(`
                                        <tr>
                                            <td>${data.username}</td>
                                            <td>${data.first_name}</td>
                                            <td>${data.last_name}</td>
                                            <td>${data.email}</td>
                                            <td>${mo} ${da}, ${ye} ${time}</td>
                                            <td>${data.allowedProjects}</td>
                                            <td class="my-auto">
                                                <button id="active_user_${data.id}" data-user_id="${data.id}" class="btn deactivate-btn text-white btn-revelation-primary">Deactivate</button>
                                            </td>
                                        </tr>
                                    `);
                                })
                            }
                        })
                        .catch(function(data){
                            console.log(data);
                            hideLoader();
                        })
                });

                //** DELETE USER CODE **//
                $(document).on('click', '.delete-btn', function(){
                    const id = $(this).data('user_id');
                    showLoader();
                    $.post(`/all-users/delete`, { user_id: id })
                        .done(function(data) {

                            hideLoader();

                            if(data.error) {

                                Swal.fire({
                                    title: 'Permissions Required',
                                    text: 'You do not have permission to perform this action.',
                                    icon: 'error',
                                    confirmButtonText: 'OK',
                                });

                            } else {
                                $('#delete_user_' + id).parents('tr').fadeOut(300, 'linear', function(){
                                    $(this).remove();
                                })
                            }
                        })
                        .catch(function(data){
                            console.log(data);
                            hideLoader();
                        })
                });                

                $('#projects').select2({ width: '100%'});
                $('#departments').select2({ width: '100%'});
                $('#permissions').select2({ width: '100%'});
                $('#locations').select2({ width: '100%'});

            });

            function saveUser()
            {
                const first_name = $('#first_name').val();
                const last_name  = $('#last_name').val();
                const email      = $('#email').val();
                const username   = $('#username').val();
                const password   = $('#password').val();
                let projects     = [];
                let permissions  = [];
                let departments  = [];
                let locations    = [];
                const userId     = $('#user_id').val();


                $.each($('#projects').select2('data'), function(){
                    projects.push(this.id);
                });

                $.each($('#departments').select2('data'), function(){
                    departments.push(this.text);
                });

                $.each($('#permissions').select2('data'), function(){
                    permissions.push(this.text);
                });

                $.each($('#locations').select2('data'), function(){
                    locations.push(this.text);
                });

                if(!first_name) {
                    Swal.fire({ title: 'Missing Field', text: 'First name is required.' }, function(){ setTimeout(function(){ $('#first_name').focus(); }) });
                    return;
                } else if(!last_name) {
                    Swal.fire({ title: 'Missing Field', text: 'Last name is required.' }, function(){ setTimeout(function(){ $('#last_name').focus(); }) });
                    return;
                } else if(!first_name) {
                    Swal.fire({ title: 'Missing Field', text: 'Email address is required.' }, function(){ setTimeout(function(){ $('#email').focus(); }) });
                    return;
                } else if(!username) {
                    Swal.fire({ title: 'Missing Field', text: 'Username is required.' }, function(){ setTimeout(function(){ $('#username').focus(); }) });
                    return;
                } else if(!password) {
                    if (!userId) {
                        Swal.fire({ title: 'Missing Field', text: 'Password is required.' }, function(){ setTimeout(function(){ $('#password').focus(); }) });
                        return;
                    }
                };

                showLoader();

                if(userId)
                    updateUser(first_name, last_name, email, password, username, projects, permissions, departments, locations, userId);
                else
                    createUser(first_name, last_name, email, password, username, projects, permissions, departments, locations);
            }

            function createUser(first_name, last_name, email, password, username, projects, permissions, departments, locations)
            {
                $.post('/users/create', { first_name, last_name, email, password, username, projects, permissions, departments, locations, 'page': 'all_users' })
                    .done(function(data){
                        hideLoader();
                        $("#addNewUser").modal('hide');
                        Swal.fire("User Created", "This user is registered successfully." , "success");
                        addUserToTable(data)
                    })
                    .fail(handleError);
            }

            function handleError(data, text, error)
            {

                hideLoader();
                let messages = JSON.parse(data.responseText);
                let str_msg = 'The following errors occured. Please adjust and try again.\r\n';
                for(const message in messages.errors)
                {
                    str_msg += messages.errors[message][0] + '\r\n';
                }

                Swal.fire({
                    title: "Oops, something went wrong",
                    text: str_msg,
                    icon: "error",
                    button: "OK",
                });
            }

            function addUserToTable(data)
            {
                const lastLogin = new Date(data.last_login);
                let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(lastLogin);
                let mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(lastLogin);
                let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(lastLogin);
                let time = new Intl.DateTimeFormat('en', { hour: 'numeric', minute: 'numeric' }).format(lastLogin);

                $('#active_user_table').append(`
                    <tr>
                        <td>${data.username}</td>
                        <td>${data.first_name}</td>
                        <td>${data.last_name}</td>
                        <td>${data.email}</td>
                        <td>${mo} ${da}, ${ye} ${time}</td>
                        <td>${data.allowedProjects}</td>
                        <td class="my-auto">
                            <button id="active_user_${data.id}" data-user_id="${data.id}" class="btn deactivate-btn text-white btn-revelation-primary">Deactivate</button>
                        </td>
                    </tr>
                `);
            }
        </script>
    @endif

    @include('partials.loader')
@endsection
