<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">


</head>

<body>
    @extends('layouts.reports')
    @section('content')
        <div class="w-3/4 rounded mx-auto mt-8" style="background-color : #337ab7;">
            <h1 class="text-white text-xl text-survey px-8 py-4 font-bold">{{$data['survey']->survey_name}}</h1>
        </div>
    <div class="w-3/4 mx-auto">
        <div class="max-w-7xl mx-auto">
            <div class="my-8 p-8 dark:border-gray-700 bg-gray-50 border border-gray-300 dark:text-gray-400 dark:bg-gray-800 overflow-hidden sm:rounded-lg">
                <div class="border-gray-200">
                    <h3 class="pb-4 text-2xl text-survey dark:text-white font-bold">Status</h3>
                    <div>
                        <div class="flex items-center my-8 text-lg">
                            <div class="text-survey font-bold w-60">Survey is Currently:</div>
                            <div class="active-status font-bold {{ $survey->survey_active == 1 ? 'text-support-activity' : 'text-red-700' }} m-0">{{$data['survey_active']}}</div>
                            <button id="activeBtn" class="btn btn-sm btn-revelation-primary ml-10" onclick="toggleActivation({{ $survey->survey_id }}, 1);" style="{{ $survey->survey_active == 1 ? 'display:none;' : '' }}">Click To Activate</button>
                            <button id="deactiveBtn" class="btn btn-sm btn-revelation-primary ml-10" onclick="toggleActivation({{ $survey->survey_id }}, 0);" style="{{ $survey->survey_active == 1 ? '' : 'display:none;' }}">Click To Deactivate</button>
                        </div>
                        <div class="flex items-center my-8 text-lg">
                            <div class="font-bold text-survey w-60">Response Rate :</div>
                            <div class="font-bold text-survey m-0">{{$data['total_resp']}} of {{$data['invitations_sent']}} ({{$data['percent_total']}}%)</div>
                        </div>
                        <div class="flex items-center my-8 text-lg">
                            <div class="font-bold text-survey w-60">Completion Rate :</div>
                            <div class="text-survey font-bold m-0">{{$data['completed_resp']}} of {{$data['total_resp']}} ({{$data['percent_completed']}}%)</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function toggleActivation(survey_id, survey_active) {
            $.ajax({
                url: "{{ route('survey.toggle-active') }}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "survey_id": survey_id,
                    "survey_active": survey_active,
                },
                dataType: 'json',
                success: function (res) {
                    if (res == 200) {
                        if (survey_active == 1) {
                            $('.active-status').removeClass('text-red-700');
                            $('.active-status').addClass('text-support-activity');
                            $('.active-status').html('Active');
                            $('#activeBtn').hide();
                            $('#deactiveBtn').show();
                        } else {
                            $('.active-status').removeClass('text-support-activity');
                            $('.active-status').addClass('text-red-700');
                            $('.active-status').html('Inactive');
                            $('#activeBtn').show();
                            $('#deactiveBtn').hide();
                        }
                    }
                },
                error: function(request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        }
    </script>
    @endsection
</body>

</html>