@extends('layouts.reports')
@section('content')
    <div class="flex justify-between items-center border-b">
        <h3 class="text-survey font-bold py-4 px-8 m-0 text-lg">Function At-A-Glance / {{ $survey->survey_name }}</h3>
        <div>
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyExport', $survey))
                {{-- <button class="btn btn-secondary" id="downloadExcel">Export to Excel</button> --}}
            @endif
            @if (\Auth::check() && \Auth::user()->hasPermission('surveyPrint', $survey))                
                <button class="btn btn-revelation-primary mr-4 ml-2" id="pdfBtn">Download PDF</button>                
            @endif
        </div>
    </div>
    <div id="individualContent">
        <div class="first_part">
            @include('analysis.partial.ataglancefilter')
        </div>
        <link rel="stylesheet" href="{{ asset('css/report-additional-style.css') }}">
        <div class="row second_part" style="padding:20px;border-top:1px solid #dfdfdf;">
            <div class="flex items-center justify-start" style="padding-left:25px;font-weight: bold;">
                Show &nbsp;&nbsp;
                <select class="select-tiny filter-secondary" name="depthQuestion" id="depthQuestion">
                    <option value="0">Legal | Support</option>
                    <option value="1">Classifications</option>
                    <option value="2">Substantive Areas</option>
                    <option value="3">Processes</option>
                    <option value="4" selected>Activities</option>
                </select>
                &nbsp;&nbsp;where the percent of hours is at least &nbsp;&nbsp;
                <div class="input-group suffix" style="width: 100px;">
                    <input class="input-tiny filter-secondary" id="minPercent" type="number" value="15" step="1">
                    <span class="input-group-addon">%</span>
                </div>
                &nbsp;&nbsp;for each &nbsp;&nbsp;
                <select class="select-tiny filter-secondary" name="filterResp" id="filterResp">
                    <option value="0">Position</option>
                    <option value="1">Department</option>
                    <option value="2">Group</option>
                    <option value="3" selected>Location</option>
                    <option value="4">Category</option>
                    <option value="5">Participant</option>
                </select>
            </div>
        </div>
        <div class="third_part" style="padding-top: 0;border-top: 3px solid #bfbfbf;">
            <div class="tableContainer">
                <table class="table" style="margin: 20px;width: 96%;">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: right;">Hours</th>
                            <th style="text-align: right;">% Hours</th>
                            <th style="text-align: right;">Cost</th>
                        </tr>
                        <tr>
                            <th>Grand Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: right;">{{ number_format(round($data['grand_total_hours']), 0, '.', ',') }}</th>
                            <th></th>
                            <th style="text-align: right;">${{ number_format(round($data['grand_total_cost']), 0, '.', ',') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['ataglance_data'] as $row)
                            <tr>
                                @if ($row['rowspan'] != 0)
                                    <td rowspan="{{ $row['rowspan'] }}" style="font-weight:bold;">{{ $row['option'] }}</td>
                                @endif
                                @php
                                    $questionDescAry = explode("..", $row['question_desc']);
                                @endphp
                                @foreach ($questionDescAry as $i => $question_desc)
                                    <td class="questionDescTD{{ $i }}" data-option="{{ $row['option'] }}">{{ $question_desc }}</td>
                                @endforeach
                                <td style="text-align: right;">{{ number_format($row['hours']) }}</td>
                                <td style="text-align: right;">{{ $row['percent'] }}%</td>
                                <td style="text-align: right;">${{ number_format($row['cost']) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="copyright_div" class="flex justify-begin items-center" style="width:100%;background-color:white;padding:10px;font-size:11px;">
        <div>
            <img src="{{asset('imgs/logo-new-small_rev.png')}}" style="height:60px" alt="">
        </div>
        <div>
            <a href="http://www.revelationlegal.com">http://www.revelationlegal.com</a> <br>
            <span>&copy; ofPartner LLC, All Rights Reserved. Report Generated @php echo date('m/d/Y') @endphp</span>
        </div>
    </div>
    <div id="headerDiv" style="background-color: white;height:40px;width:100%;"></div>
    <div id="pdfPreview">
        <div id="imgReport1"></div>
        <div id="imgReport2" style="background:white;"></div>
        <div id="imgReport3" style="background: white;"></div>
        <div id="copyImg"></div>
    </div>
    <div class="loading-mask"></div>
    <script>
        var survey_id = @php echo $data['survey'] -> survey_id; @endphp;
        var respData  = @php echo $data['resps']; @endphp;

        let formatter = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'USD',
                        minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
                        maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
                    });

        let numberFormatter = new Intl.NumberFormat('en-US');

        // Handle the pdf button click, generate image data from the body
        $('#pdfBtn').click(function () {
            $('#pdfBtn').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Generating PDF...`);
            $('#pdfBtn').prop('disabled', true);
            source = $('#individualContent .first_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    canvas.setAttribute('id', 'imgCanvas1');
                    $('#imgReport1').append(canvas);
                }
            });
            source = $('#individualContent .second_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    canvas.setAttribute('id', 'imgCanvas2')
                    $('#imgReport2').append(canvas);
                }
            });
            // Copyright
            source = $('#copyright_div');
            html2canvas(source, {
                onrendered: function (canvas) {
                    canvas.setAttribute('id', 'copyrightCanvas');
                    $('#copyImg').append(canvas);
                }
            });
            source = $('#headerDiv');
            html2canvas(source, {
                onrendered: function (canvas) {
                    canvas.setAttribute('id', 'headerCanvas');
                    $('#copyImg').append(canvas);
                }
            });
            source = $('#individualContent .third_part');
            html2canvas(source, {
                onrendered: function (canvas) {
                    canvas.setAttribute('id', 'imgCanvas3')
                    $('#imgReport3').append(canvas);

                    setTimeout(() => {
                        let imgWidth = $('#imgReport1').outerWidth();
                        imgData_1 = document.getElementById('imgCanvas1').toDataURL('image/jpeg', 1.0);
                        imgData_2 = document.getElementById('imgCanvas2').toDataURL('image/jpeg', 1.0);
                        imgData_3 = document.getElementById('imgCanvas3').toDataURL('image/jpeg', 1.0);
                        copyrightData = document.getElementById('copyrightCanvas').toDataURL('image/jpeg', 1.0);
                        headerData = document.getElementById('headerCanvas').toDataURL('image/jpeg', 1.0);
                        pdfdoc = new jsPDF('p', 'mm', 'a4');
                        imgHeight1 = Math.round($('#imgReport1').outerHeight() * 190 / imgWidth);
                        y = 10;
                        position = y;
                        doc_page = 1;

                        pdfdoc.addImage(imgData_1, 'JPEG', 10, y, 190, imgHeight1);
                        y += imgHeight1;

                        imgHeight2 = Math.round($('#imgReport2').outerHeight() * 190 / imgWidth);
                        pdfdoc.addImage(imgData_2, 'JPEG', 10, y, 190, imgHeight2);
                        y += imgHeight2;

                        imgHeight3 = Math.round($('#imgReport3').outerHeight() * 190 / imgWidth);
                        pdfdoc.addImage(imgData_3, 'JPEG', 10, y, 190, imgHeight3);
                        y += imgHeight3;

                        pageHeight = pdfdoc.internal.pageSize.height - 20;
                        heightLeft = y - pageHeight;

                        while (heightLeft >= -pageHeight) {
                            position = heightLeft - imgHeight3;
                            pdfdoc.addPage();
                            doc_page++;
                            pdfdoc.addImage(imgData_3, 'JPEG', 10, position, 190, imgHeight3);
                            heightLeft -= pageHeight;
                        }

                        pdfdoc.deletePage(doc_page);

                        for (i = 1; i < doc_page; i++) {
                            pdfdoc.setPage(i);
                            pdfdoc.addImage(headerData, 'JPEG', 10, 0, 190, 10);
                            pdfdoc.addImage(copyrightData, 'JPEG', 10, 287, 190, 10);
                        }

                        pdfdoc.save(`Function At-A-Glance({{$data['survey']->survey_name}})`);
                        $('#imgCanvas1').remove();
                        $('#imgCanvas2').remove();
                        $('#imgCanvas3').remove();
                        $('#copyrightCanvas').remove();
                        $('#headerCanvas').remove();
                        $('#pdfBtn').html('Download PDF');
                        $('#pdfBtn').prop('disabled', false);
                    }, 1000);
                }
            });
        });

        $('.filter-secondary').change(function () {
            let depthQuestion = $('#depthQuestion').val();
            let minPercent    = $('#minPercent').val();
            let filterResp    = $('#filterResp').val();

            $.ajax({
                url: '{{ route("getAtAGlanceTableData") }}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "survey_id": survey_id,
                    "position": JSON.stringify(options['position']),
                    "department": JSON.stringify(options['department']),
                    "group": JSON.stringify(options['group']),
                    "category": JSON.stringify(options['category']),
                    "location": JSON.stringify(options['location']),
                    "depthQuestion": depthQuestion,
                    "minPercent": minPercent,
                    "filterResp": filterResp
                },
                dataType: 'json',
                beforeSend: function () {
                    mask_height = $('body').height();
                    $('.loading-mask').css('height', mask_height);
                    $('.loading-mask').show();
                },
                success: function (res) {
                    rows = res.ataglance_data;
                    $tableContainer = $('.tableContainer');
                    strHtml = `<table class="table" style="margin: 20px;width: 96%;">
                                    <thead>
                                        <tr>
                                            <th></th>`;
                    for (i = 0; i <= depthQuestion; i++) {
                        strHtml += `<th></th>`;
                    }
                    strHtml +=  `           <th style="text-align: right;">Hours</th>
                                            <th style="text-align: right;">% Hours</th>
                                            <th style="text-align: right;">Cost</th>
                                        </tr>
                                        <tr>
                                            <th>Grand Total</th>`;
                    for (i = 0; i <= depthQuestion; i++) {
                        strHtml += `<th></th>`;
                    }
                    strHtml +=  `           <th style="text-align: right;">${ numberFormatter.format(res.grand_total_hours) }</th>
                                            <th></th>
                                            <th style="text-align: right;">${ formatter.format(res.grand_total_cost) }</th>
                                        </tr>
                                    </thead>
                                    <tbody>`;

                    rows.forEach(row => {
                        strHtml += `<tr>`;
                        if (row.rowspan != 0) {
                            strHtml += `<td rowspan="${row.rowspan}" style="font-weight:bold;">${ row.option }</td>`;
                        }
                        questionDescAry = row.question_desc.split("..");
                        for (i = 0; i < questionDescAry.length; i++) {
                            strHtml += `<td class="questionDescTD${i}" data-option="${row.option}">${questionDescAry[i]}</td>`;
                        }
                        strHtml += `    <td style="text-align: right;">${ numberFormatter.format(row.hours) }</td>
                                        <td style="text-align: right;">${ row.percent }%</td>
                                        <td style="text-align: right;">${ formatter.format(row.cost) }</td>
                                    </tr>`;
                    });

                    strHtml += `</tbody>
                            </table>`;

                    $tableContainer.html(strHtml);

                    for (let i = 0; i < depthQuestion; i++) {
                        var span = 1;
                        var prevTD = "";
                        var prevTDVal = "";
                        var prevTDOption = "";
                        $(`td.questionDescTD${i}`).each(function() {
                            var $this = $(this);
                            if ($this.text() == prevTDVal && $this.attr('data-option') == prevTDOption) { // check value of previous td text
                                span++;
                                if (prevTD != "") {
                                    prevTD.attr("rowspan", span); // add attribute to previous td
                                    $this.remove(); // remove current td
                                }
                            } else {
                                prevTD     = $this; // store current td
                                prevTDVal  = $this.text();
                                prevTDOption  = $this.attr('data-option');
                                span       = 1;
                            }
                        });
                    }

                    $('.loading-mask').hide();
                },
                error: function(request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        });

        $('#downloadExcel').click(function () {
            let depthQuestion = $('#depthQuestion').val();
            let minPercent    = $('#minPercent').val();
            let filterResp    = $('#filterResp').val();

            $.ajax({
                url: '{{ route("getAtAGlanceExcelExport") }}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "survey_id": survey_id,
                    "position": JSON.stringify(options['position']),
                    "department": JSON.stringify(options['department']),
                    "group": JSON.stringify(options['group']),
                    "category": JSON.stringify(options['category']),
                    "location": JSON.stringify(options['location']),
                    "depthQuestion": depthQuestion,
                    "minPercent": minPercent,
                    "filterResp": filterResp
                },
                dataType: 'json',
                beforeSend: function () {
                    $('#downloadExcel').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Generating Excel File...`);
                    $('#downloadExcel').prop('disabled', true);
                },
                success: function (res) {
                    location.href = res.url;
                    $('#downloadExcel').html(`Export to Excel`);
                    $('#downloadExcel').prop('disabled', false);
                },
                error: function(request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        });

        $(document).ready(function () {
            for (let i = 0; i < 5; i++) {
                var span = 1;
                var prevTD = "";
                var prevTDVal = "";
                var prevTDOption = "";
                $(`td.questionDescTD${i}`).each(function() {
                    var $this = $(this);
                    if ($this.text() == prevTDVal && $this.attr('data-option') == prevTDOption) { // check value of previous td text
                        span++;
                        if (prevTD != "") {
                            prevTD.attr("rowspan", span); // add attribute to previous td
                            $this.remove(); // remove current td
                        }
                    } else {
                        prevTD     = $this; // store current td
                        prevTDVal  = $this.text();
                        prevTDOption  = $this.attr('data-option');
                        span       = 1;
                    }
                });
            }
        });
    </script>

@endsection
